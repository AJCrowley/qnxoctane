package com.lixar.components
{
	import com.lixar.components.mediaplayer.MediaConstants;
	import com.lixar.skins.HSliderSkin;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.core.IFlexDisplayObject;
	import mx.core.InteractionMode;
	import mx.events.FlexEvent;
	import mx.events.ResizeEvent;
	
	import spark.components.Button;
	import spark.components.HSlider;
	import spark.effects.animation.Animation;
	
	[Style(name="showTrackHighlight", type="Boolean", inherit="no")]
	[Event(name="trackClicked", type="flash.events.MouseEvent")]
	
	public class HTrackingSlider extends HSlider
	{
		private var animator:Animation = null;
		
		[SkinPart(required="false")]
		public var trackHighLight:Button;
		
		[Bindable]
		private var _accentColor:uint;
		private var accentColorChanged:Boolean
		
		[Bindable]
		private var _showTrackHighlight:Boolean = true;
		private var showTrackHighlightChanged:Boolean;
		
		public function HTrackingSlider():void
		{
			super();
			setStyle("skinClass", HSliderSkin);
		}
		
		override protected function updateSkinDisplayList():void
		{
			super.updateSkinDisplayList();
			if (!thumb || !track || !trackHighLight)
			{
				return;
			}			
			var thumbRange:Number = track.getLayoutBoundsWidth() - thumb.getLayoutBoundsWidth();
			var range:Number = maximum - minimum;
			// calculate new thumb position.
			var thumbPosTrackX:Number = (range > 0) ? ((pendingValue - minimum) / range) * thumbRange : 0;
			// convert to parent's coordinates.
			var thumbPos:Point = track.localToGlobal(new Point(thumbPosTrackX, 0));
			var thumbPosParentX:Number = thumb.parent.globalToLocal(thumbPos).x + thumb.getLayoutBoundsWidth() / 2;
			trackHighLight.setLayoutBoundsSize(Math.round(thumbPosParentX), trackHighLight.getLayoutBoundsHeight());
		}
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			switch(partName)
			{
				case "track":
					track.addEventListener
					(
						MouseEvent.CLICK,
						function(event:MouseEvent):void
						{
							var clickEvent:MouseEvent = new MouseEvent(MediaConstants.EVENT_SLIDER_TRACK_CLICKED, true);
							clickEvent.localX = event.localX;
							clickEvent.localY = event.localY;
							dispatchEvent(clickEvent);
						}
					);
					break;
				
				case "trackHighLight":
					trackHighLight.addEventListener
					(
						MouseEvent.CLICK,
						function(event:MouseEvent):void
						{
							var clickEvent:MouseEvent = new MouseEvent(MediaConstants.EVENT_SLIDER_TRACK_CLICKED, true);
							clickEvent.localX = event.localX;
							clickEvent.localY = event.localY;
							dispatchEvent(clickEvent);
						}
					);
					break;
			}
			if (instance == trackHighLight)
			{
				trackHighLight.focusEnabled = false;
				trackHighLight.addEventListener(ResizeEvent.RESIZE, trackHighLight_resizeHandler);
				// track is only clickable if in mouse interactionMode
				if (getStyle("interactionMode") == InteractionMode.MOUSE)
				{
					trackHighLight.addEventListener(MouseEvent.MOUSE_DOWN, trackHighLight_mouseDownHandler);
				}
			}
		}
		
		override protected function partRemoved(partName:String, instance:Object):void
		{
			super.partRemoved(partName, instance);
			if (instance == trackHighLight)
			{
				trackHighLight.removeEventListener(MouseEvent.MOUSE_DOWN, trackHighLight_mouseDownHandler);
				trackHighLight.removeEventListener(ResizeEvent.RESIZE, trackHighLight_resizeHandler);
			}
		}
		
		protected function trackHighLight_mouseDownHandler(event:MouseEvent):void
		{
			this.track_mouseDownHandler(event);
		}
		
		private function trackHighLight_resizeHandler(event:Event):void
		{
			updateSkinDisplayList();
		}
		
		override public function styleChanged(styleProp:String):void
		{
			var anyStyle:Boolean = styleProp == null || styleProp == "styleName";			
			super.styleChanged(styleProp);
			if (styleProp == "showTrackHighlight" || anyStyle)
			{
				showTrackHighlightChanged = true;
				invalidateProperties();
			}			
			if (styleProp == "accentColor" || anyStyle)
			{
				accentColorChanged = true;
				invalidateProperties();
			}			
			invalidateDisplayList();
		}
		
		override protected function commitProperties():void
		{
			super.commitProperties();			
			if (showTrackHighlightChanged)
			{
				this.trackHighLight.visible = this._showTrackHighlight;
				showTrackHighlightChanged = false;
			}
			if(accentColorChanged){
				this.trackHighLight.setStyle("themeColor", this.accentColor);
				accentColorChanged = false;
			}
		}
		
		public function set accentColor(color:uint):void
		{
			this._accentColor = color;
			accentColorChanged = true;
			this.invalidateProperties();
		}
		
		
		public function get accentColor():uint
		{
			return this._accentColor;
		}
		
		public function set showTrackHighlight(show:Boolean):void
		{
			this._showTrackHighlight = show;
			showTrackHighlightChanged = true;
			this.invalidateProperties();
		}
		
		public function get showTrackHighlight():Boolean
		{
			return this._showTrackHighlight;
		}

	}
}