package com.lixar.components.mediaplayer
{
	import com.adobe.air.filesystem.FileMonitor;
	import com.adobe.air.filesystem.events.FileMonitorEvent;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import qnx.events.PPSEvent;
	import qnx.pps.PPS;

	public class PPSDeviceInfo extends EventDispatcher
	{
		private var _ppsFile:File;
		private var _ppsFileMonitor:FileMonitor;
		private var _ppsChannel:PPS;

		protected function get ppsChannel():PPS
		{
			return this._ppsChannel;
		}
		
		public function PPSDeviceInfo(ppsFile:File)
		{
			this._ppsFile = ppsFile;
			
			// Create the PPS Channel instance
			this._ppsChannel = new PPS();
			
			// Create the listeners for changes to the PPS channel
			this._ppsChannel.addEventListener(PPSEvent.PPS_OPENED, this.handlePPSInfoChanged);
			this._ppsChannel.addEventListener(PPSEvent.PPS_CHANGED, this.handlePPSInfoChanged);
			
			// In the event that the PPS file is removed, we want to start the water immediately
			this._ppsChannel.addEventListener(PPSEvent.PPS_REMOVED, this.handlePPSRemoved);
			
			// Check if the file exists
			if(ppsFile.exists)
			{
				// Open it
				this.openPPSChannel();
			}

			else
			{
				// Start the file monitor
				this.startPPSFileMonitor();
			}
		}
		
		private function openPPSChannel(event:FileMonitorEvent=null):void
		{
			this._ppsChannel.openAsync(this._ppsFile.nativePath, PPS.O_RDONLY);
		}
		
		protected function handlePPSInfoChanged(event:PPSEvent):void
		{
			// Should be overridden in the subclass
		}
		
		private function handlePPSRemoved(event:PPSEvent):void
		{
			// Close the existing channel
			this._ppsChannel.close();
			
			// Start the file monitor
			this.startPPSFileMonitor();
		}
		
		private function startPPSFileMonitor():void
		{
			this._ppsFileMonitor = new FileMonitor();
			this._ppsFileMonitor.addEventListener(FileMonitorEvent.CREATE, openPPSChannel);
			this._ppsFileMonitor.addEventListener(FileMonitorEvent.CHANGE, openPPSChannel);
			this._ppsFileMonitor.file = this._ppsFile;
			this._ppsFileMonitor.watch();
		}
		
		private function stopPPSFileMonitor():void
		{
			this._ppsFileMonitor.removeEventListener(FileMonitorEvent.CREATE, openPPSChannel);
			this._ppsFileMonitor.removeEventListener(FileMonitorEvent.CHANGE, openPPSChannel);
			this._ppsFileMonitor.unwatch();
			this._ppsFileMonitor = null;
		}
		
		public function close():void
		{
			if(this._ppsChannel)
			{
				this._ppsChannel.close();
			}
			if(this._ppsFileMonitor)
			{
				this.stopPPSFileMonitor();
			}
		}
	}
}