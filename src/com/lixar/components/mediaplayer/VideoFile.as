package com.lixar.components.mediaplayer
{
	import com.lixar.qnx.octane.view.resources.ImageResource;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.filesystem.File;

	[Bindable]
	public class VideoFile extends MediaFile
	{
		public var artist:String = "";
		public var width:uint;
		public var height:uint;
		
		public function VideoFile()
		{
			super();
			artwork = "resources/default_video_thumbnail.png";
		}
	}
}