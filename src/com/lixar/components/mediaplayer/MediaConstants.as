package com.lixar.components.mediaplayer
{
	public class MediaConstants
	{
		public static const QNET_BASE_DIR:String = "/net";
		public static const MEDIA_LIBRARY_PATH:String = "/accounts/1000/db/mmlibrary.db";
		public static const CATEGORY_DEVICE:uint = 0;
		public static const CATEGORY_MUSIC_ARTISTS:uint = 1;
		public static const CATEGORY_MUSIC_ALBUMS:uint = 2;
		public static const CATEGORY_ALL_MUSIC:uint = 3;
		public static const CATEGORY_ARTIST:uint = 4;
		public static const CATEGORY_ALBUM:uint = 5;
		public static const CATEGORY_ALL_VIDEOS:uint = 6;
		
		// PPS
		public static const OCTANE_MEDIA_PPS_PATH:String = "/pps/system/octane/mediaplayer/";
		public static const OCTANE_MEDIA_PPS_FILE:String = "player";
		public static const OCTANE_MEDIA_PPS_NOWPLAYING_FILE:String = "nowplaying";
		public static const OCTANE_MEDIA_PPS_CONTROLLER:String = "control";
		public static const NOW_PLAYING_PPS_PATH:String = "/pps/services/multimedia/renderer/context/[INSTANCEID]/q1";
		public static const PLAYER_STATE_PPS_PATH:String = "/pps/services/multimedia/renderer/context/[INSTANCEID]/state";
		public static const PLAYER_STATUS_PPS_PATH:String = "/pps/services/multimedia/renderer/context/[INSTANCEID]/status";
		public static const MEDIA_RENDERER_PPS_PATH:String = "/pps/services/multimedia/renderer/context/";
		public static const INVOKE_COMMAND_PPS_PATH:String = "/pps/services/navigator/control";
		public static const VOLUME_STATUS_PPS_PATH:String = "/pps/services/audio/status";
		
		// media types
		// TODO: Can we use the MediaPlayerMediaType constants for these instead?
		public static const MEDIA_TYPE_AUDIO:String = "Audio";
		public static const MEDIA_TYPE_VIDEO:String = "Video";
		public static const MEDIA_TYPE_NONE:String = "Not Playing";
		
		// audio controller events
		public static const EVENT_PLAYLIST_COMPLETE:String = "playlistcomplete";
		public static const EVENT_TOGGLE_PAUSE:String="togglePause";
		
		// tracking slider events
		public static const EVENT_SLIDER_TRACK_CLICKED:String = "trackClicked";
		
		// device events
		public static const EVENT_LOCAL_DEVICE_UPDATED:String = "localDeviceUpdated";
		public static const EVENT_TARGET_DEVICE_UPDATED:String = "targetDeviceUpdated";
		public static const EVENT_DEVICE_REMOTE_PLAYBACK_COMPLETE:String = "deviceRemotePlaybackComplete";
		
		// constants for remote commands
		public static const INVOKE_COMMAND:String = "invoke";
		public static const INVOKE_DATA:String = "dat";
		public static const INVOKE_URL_PROTOCOL:String = "octanemedia://";
		public static const INVOKE_COMMAND_SENT:String = "invokeSent";
		public static const REMOTE_COMMAND_DELIMITER:String = ":";
		public static const REMOTE_COMMAND_LIST_DELIMITER:String = "|";
		public static const REMOTE_COMMAND_PLAY:String = "play";
		public static const REMOTE_COMMAND_PLAY_ARTIST:String = "playArtist";
		public static const REMOTE_COMMAND_PLAY_ALBUM:String = "playAlbum";
		public static const REMOTE_COMMAND_PAUSE:String = "pause";
		public static const REMOTE_COMMAND_NEXT:String = "next";
		public static const REMOTE_COMMAND_PREV:String = "prev";
		public static const REMOTE_COMMAND_STOP:String = "stop";
		public static const REMOTE_COMMAND_SEEK:String = "seek";
		public static const REMOTE_COMMAND_SET_VOLUME:String = "setVolume";

		// remote seek command
		public static const REMOTE_COMMAND_SEEK_POSITION:uint = 0;
		
		// remote volume command
		public static const REMOTE_COMMAND_SET_VOLUME_LEVEL:uint = 0;

		// remote play command
		public static const REMOTE_COMMAND_PLAY_SOURCE_DEVICE_QNET_NAME:uint = 0;
		public static const REMOTE_COMMAND_PLAY_MEDIA_TYPE:uint = 1;
		public static const REMOTE_COMMAND_PLAY_TRACK_LIST:uint = 2;
		public static const REMOTE_COMMAND_PLAY_CURRENT_TRACK:uint = 3;

		// remote play artist command
		public static const REMOTE_COMMAND_PLAY_ARTIST_SOURCE_DEVICE_QNET_NAME:uint = 0;
		public static const REMOTE_COMMAND_PLAY_ARTIST_ID:uint = 1;

		// remote play album command
		public static const REMOTE_COMMAND_PLAY_ALBUM_SOURCE_DEVICE_QNET_NAME:uint = 0;
		public static const REMOTE_COMMAND_PLAY_ALBUM_ID:uint = 1;

		public function MediaConstants()
		{
		}
	}
}