package com.lixar.components.mediaplayer
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import mx.states.OverrideBase;
	
	import qnx.events.PPSEvent;
	import qnx.pps.PPS;
	import qnx.system.AudioInput;
	import qnx.system.AudioOutput;

	public class DeviceVolumeInfo extends PPSDeviceInfo
	{
		public static const VOLUME_CHANGED:String = "volumeChanged";
		public static const MUTE_CHANGED:String = "muteChanged";
		
		private var _availableOutput:String;
		private var _speakerMuted:Boolean;
		private var _speakerVolume:Number;
		private var _headphoneMuted:Boolean;
		private var _headphoneVolume:Number;

		[Bindable(event="muteChanged")]
		public function get isMuted():Boolean
		{
			var isMuted:Boolean = false;
			switch(this._availableOutput)
			{
				case AudioOutput.HEADPHONES:
					isMuted = this._headphoneMuted;
					break;
				case AudioOutput.SPEAKERS:
					isMuted = this._speakerMuted;
					break;
			}
			
			return isMuted;
		}

		[Bindable(event="volumeChanged")]
		public function get volume():Number
		{	
			var volume:Number = 0;
			switch(this._availableOutput)
			{
				case AudioOutput.HEADPHONES:
					volume = this._headphoneVolume;
					break;
				case AudioOutput.SPEAKERS:
					volume = this._speakerVolume;
					break;
			}
			
			return volume;
		}

		public function DeviceVolumeInfo(basePath:String)
		{
			super(new File(basePath + MediaConstants.VOLUME_STATUS_PPS_PATH));
		}
		
		override protected function handlePPSInfoChanged(event:PPSEvent):void
		{
			// We'll consider the volume or mute to have changed if the available
			// output has changed, OR if the volume/mute status for the current output
			// has changed
			var volumeChanged:Boolean = false;
			var muteChanged:Boolean = false;
			
			if(this._availableOutput != (event.currentTarget as PPS).data["output.available"])
			{
				this._availableOutput = (event.currentTarget as PPS).data["output.available"];
				volumeChanged = true;
				muteChanged = true;
			}

			if(this._headphoneMuted != Boolean((event.currentTarget as PPS).data["output.headphone.muted"]) &&
				this._availableOutput == AudioOutput.HEADPHONES)
			{
				this._headphoneMuted = Boolean((event.currentTarget as PPS).data["output.headphone.muted"]);
				muteChanged = true;
			}
			
			if(this._headphoneVolume != Boolean((event.currentTarget as PPS).data["output.headphone.volume"]) &&
				this._availableOutput == AudioOutput.HEADPHONES)
			{
				this._headphoneVolume = Number((event.currentTarget as PPS).data["output.headphone.volume"]);
				volumeChanged = true;
			}

			if(this._speakerMuted != Boolean((event.currentTarget as PPS).data["output.speaker.muted"]) &&
				this._availableOutput == AudioOutput.SPEAKERS)
			{
				this._speakerMuted = Boolean((event.currentTarget as PPS).data["output.speaker.muted"]);
				muteChanged = true;
			}
			
			if(this._speakerVolume != Boolean((event.currentTarget as PPS).data["output.speaker.volume"]) &&
				this._availableOutput == AudioOutput.SPEAKERS)
			{
				this._speakerVolume = Number((event.currentTarget as PPS).data["output.speaker.volume"]);
				volumeChanged = true;
			}

			// Dispatch the change events, if necessary
			if(volumeChanged)
			{
				this.dispatchEvent(new Event(VOLUME_CHANGED));
			}

			if(muteChanged)
			{
				this.dispatchEvent(new Event(MUTE_CHANGED));
			}
		}
	}
}