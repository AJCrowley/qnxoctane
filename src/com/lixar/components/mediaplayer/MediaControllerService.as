package com.lixar.components.mediaplayer
{
	import com.lixar.util.Singleton;
	
	import mx.collections.ArrayCollection;
	
	import qnx.events.PPSChannelEvent;
	import qnx.pps.Message;
	import qnx.pps.PPSAccess;
	import qnx.pps.PPSChannel;

	public class MediaControllerService
	{
		private var _mediaController:MediaController;
		private var _ppsController:PPSChannel;
		
		public function MediaControllerService(mediaController:MediaController)
		{
			this._mediaController = mediaController;
			
			// Start the service automatically
			this.connect();
		}
		
		public function connect():void
		{
			if(this._ppsController)
			{
				// Close existing
				this.disconnect();
			}
			
			this._ppsController = new PPSChannel();
			this._ppsController.addEventListener(PPSChannelEvent.OPEN_FAIL, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.OPEN, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.CONNECT, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.CLOSE, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.DISCONNECT, controllerEvent);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_PLAY, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_PLAY_ARTIST, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_PLAY_ALBUM, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_PAUSE, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_PREV, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_NEXT, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_STOP, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_SEEK, controllerMessage);
			this._ppsController.registerHandler(MediaConstants.REMOTE_COMMAND_SET_VOLUME, controllerMessage);
			
			this._ppsController.connect("/pps/system/octane/mediaplayer/control", true, PPSAccess.GROUP_READ_WRITE | PPSAccess.OWNER_READ_WRITE | PPSAccess.OTHER_READ_WRITE );
		}
		
		public function disconnect():void
		{
			if(this._ppsController)
			{
				this._ppsController.removeEventListener(PPSChannelEvent.OPEN_FAIL, controllerEvent);
				this._ppsController.removeEventListener(PPSChannelEvent.OPEN, controllerEvent);
				this._ppsController.removeEventListener(PPSChannelEvent.CONNECT, controllerEvent);
				this._ppsController.removeEventListener(PPSChannelEvent.CLOSE, controllerEvent);
				this._ppsController.removeEventListener(PPSChannelEvent.DISCONNECT, controllerEvent);
				this._ppsController.disconnect();
				this._ppsController = null;
			}
		}
		
		public function sendMessage(msg:String, data:*):void
		{
			if(this._ppsController)
			{
				this._ppsController.sendMsg(new Message(msg, data));
			}
		}
		
		private function controllerEvent(event:PPSChannelEvent):void
		{
			trace(event);
		}
		
		private function controllerMessage(msg:Message, cbData:String = null):void
		{
			// Get the command arguments
			var args:Array;
			if(msg.data && msg.data is String)
			{
				args = (msg.data as String).split(MediaConstants.REMOTE_COMMAND_DELIMITER);
			}
			
			switch(msg.msg)
			{
				case MediaConstants.REMOTE_COMMAND_PLAY:
					this.handleRemoteCommandPlay(args);					
					break;
				
				case MediaConstants.REMOTE_COMMAND_PLAY_ARTIST:
					this.handleRemoteCommandPlayArtist(args);
					break;
				
				case MediaConstants.REMOTE_COMMAND_PLAY_ALBUM:
					this.handleRemoteCommandPlayAlbum(args);
					break;
				
				case MediaConstants.REMOTE_COMMAND_PAUSE:
					if(this._mediaController.mediaPlayer.isPlaying)
					{
						this._mediaController.togglePause(true);
					}
					break;
				
				case MediaConstants.REMOTE_COMMAND_PREV:
					if(this._mediaController.mediaPlayer.isPlaying)
					{
						this._mediaController.prev(true);
					}
					break;
				
				case MediaConstants.REMOTE_COMMAND_NEXT:
					if(this._mediaController.mediaPlayer.isPlaying)
					{
						this._mediaController.next(true);
					}
					break;
				
				case MediaConstants.REMOTE_COMMAND_STOP:
					this._mediaController.stop(true);
					break;
				
				case MediaConstants.REMOTE_COMMAND_SEEK:
					if(args[MediaConstants.REMOTE_COMMAND_SEEK_POSITION] != null)
					{
						this._mediaController.seek(uint(args[MediaConstants.REMOTE_COMMAND_SEEK_POSITION]), true);
					}
					break;
				
				case MediaConstants.REMOTE_COMMAND_SET_VOLUME:
					if(args[MediaConstants.REMOTE_COMMAND_SET_VOLUME_LEVEL] != null)
					{
						this._mediaController.setVolumeLevel(Number(args[MediaConstants.REMOTE_COMMAND_SET_VOLUME_LEVEL]), true);
					}
					break;				
			}
		}
		
		private function handleRemoteCommandPlay(args:Array):void
		{
			// Attempt to get the source device defined in the remote command
			if(args[MediaConstants.REMOTE_COMMAND_PLAY_SOURCE_DEVICE_QNET_NAME] != null)
			{
				// Get the device
				var devices:Devices = Singleton.getInstance(Devices) as Devices;
				
				var sourceDevice:Device = devices.getDeviceByQnetName(args[MediaConstants.REMOTE_COMMAND_PLAY_SOURCE_DEVICE_QNET_NAME]);
				if(sourceDevice != null)
				{
					// Get the media library instance. We'll be reinitializing this instance based on the source device info
					var mediaLibrary:MediaLibrary = Singleton.getInstance(MediaLibrary) as MediaLibrary;
					mediaLibrary.init(sourceDevice);
					
					// Build the playlist and get the current track
					var trackList:Array = args[MediaConstants.REMOTE_COMMAND_PLAY_TRACK_LIST].split(MediaConstants.REMOTE_COMMAND_LIST_DELIMITER);
					
					var playlist:Playlist = new Playlist();
					
					// Check if these are audio or video files
					if(args[MediaConstants.REMOTE_COMMAND_PLAY_MEDIA_TYPE] == MediaConstants.MEDIA_TYPE_AUDIO)
					{
						// Build the playlist
						playlist = mediaLibrary.getPlaylist(trackList);													
						
						// Start playing
						this._mediaController.play(playlist, args[MediaConstants.REMOTE_COMMAND_PLAY_CURRENT_TRACK], true);
						
						// Note that we don't show the player here - we'll just start playing the audio in the background of
						// whatever the user is doing on the device
					}
					else if(args[MediaConstants.REMOTE_COMMAND_PLAY_MEDIA_TYPE] == MediaConstants.MEDIA_TYPE_VIDEO)
					{
						// Remote video play via control object remote command is not supported - how
						// would it be possible to start playing a video in the background?
						trace("PPS control server object '" + MediaConstants.REMOTE_COMMAND_PLAY + "' command does not support " + MediaConstants.MEDIA_TYPE_VIDEO + " media type");
					}
					else
					{
						trace("Unknown media type: '" + args[MediaConstants.REMOTE_COMMAND_PLAY_MEDIA_TYPE].toString() + "'");
					}
				}
				else
				{
					trace("Unknown source device: '" + args[MediaConstants.REMOTE_COMMAND_PLAY_SOURCE_DEVICE_QNET_NAME].toString() + "'");
				}
			}
			else
			{
				trace("Source device QNET name not provided in remote play command");
			}
		}
		
		private function handleRemoteCommandPlayArtist(args:Array):void
		{
			// Validate arguments
			if(args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_SOURCE_DEVICE_QNET_NAME] != null)
			{
				if(args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_ID] != null &&
					int(args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_ID]) > 0)
				{
					// Get the device
					var sourceDevice:Device = (Singleton.getInstance(Devices) as Devices).getDeviceByQnetName(args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_SOURCE_DEVICE_QNET_NAME]);
					if(sourceDevice != null)
					{
						this._mediaController.playArtist(sourceDevice, int(args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_ID]), true);
					}
					else
					{
						throw new Error("Unknown source device: '" + args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_SOURCE_DEVICE_QNET_NAME].toString() + "'");
					}
				}
				else
				{
					throw new Error("Artist ID not provided or contains invalid value");
				}
			}
			else
			{
				throw new Error("Source device QNET name not provided in remote play command");
			}
		}

		private function handleRemoteCommandPlayAlbum(args:Array):void
		{
			if(args[MediaConstants.REMOTE_COMMAND_PLAY_ALBUM_SOURCE_DEVICE_QNET_NAME] != null)
			{
				if(args[MediaConstants.REMOTE_COMMAND_PLAY_ALBUM_ID] != null &&
					int(args[MediaConstants.REMOTE_COMMAND_PLAY_ALBUM_ID]) > 0)
				{
					// Get the device
					var sourceDevice:Device = (Singleton.getInstance(Devices) as Devices).getDeviceByQnetName(args[MediaConstants.REMOTE_COMMAND_PLAY_ALBUM_SOURCE_DEVICE_QNET_NAME]);
					if(sourceDevice != null)
					{
						this._mediaController.playAlbum(sourceDevice, int(args[MediaConstants.REMOTE_COMMAND_PLAY_ALBUM_ID]), true);
					}
					else
					{
						throw new Error("Unknown source device: '" + args[MediaConstants.REMOTE_COMMAND_PLAY_ARTIST_SOURCE_DEVICE_QNET_NAME].toString() + "'");
					}
				}
				else
				{
					throw new Error("Album ID not provided or contains invalid value");
				}
			}
			else
			{
				throw new Error("Source device QNET name not provided in remote play command");
			}
		}
	}
}