package com.lixar.components.mediaplayer
{
	import com.lixar.util.Singleton;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import qnx.events.PPSEvent;
	import qnx.media.MediaPlayer;
	import qnx.media.VideoDisplay;
	import qnx.pps.PPS;
	
	public class MediaPlayer extends qnx.media.MediaPlayer
	{
		private var pps:PPS;
		
		public function MediaPlayer(url:String=null, display:VideoDisplay=null)
		{
			super(url, display);
			checkPPSExists();
			pps = new PPS();
			pps.addEventListener(PPSEvent.PPS_OPENED, writeInstance);
			pps.openAsync(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_FILE, PPS.O_WRONLY);
		}
		
		private function checkPPSExists():void
		{
			var file:File = new File(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_FILE);
			if(!file.exists)
			{
				var ppsDir:File = new File(MediaConstants.OCTANE_MEDIA_PPS_PATH);
				ppsDir.createDirectory();
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.WRITE);
				stream.close();
			}
		}
		
		private function writeInstance(event:PPSEvent):void
		{
			pps.write({"context": this.contextName});
		}
		
		override public function stop():void
		{
			super.stop();
		}
		
		override public function dispose():void
		{
			super.dispose();
			pps = new PPS();
			pps.open(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_FILE, PPS.O_WRONLY);
			pps.write({"context": ""});
			pps.close();
		}
	}
}