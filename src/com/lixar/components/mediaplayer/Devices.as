package com.lixar.components.mediaplayer
{
	import com.lixar.qnx.octane.mediaplayer.events.DeviceEvent;
	import com.lixar.util.Singleton;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FileListEvent;
	import flash.events.IEventDispatcher;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.net.FileReference;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;

	[Bindable]
	public class Devices extends Singleton implements IEventDispatcher
	{	
		public static const DEVICE_HEAD_NAME:String = "headunit";
		public static const DEVICE_LEFT_REAR_NAME:String = "leftrear";
		public static const DEVICE_RIGHT_REAR_NAME:String = "rightrear";
		private static const DEVICE_HEAD_FRIENDLY_NAME:String = "Center Console";
		private static const DEVICE_LEFT_REAR_FRIENDLY_NAME:String = "Left Rear Seat";
		private static const DEVICE_RIGHT_REAR_FRIENDLY_NAME:String = "Right Rear Seat";

		private var dispatcher:EventDispatcher;
		private var deviceSearchTimer:Timer;
		private var isAsyncDirOperationPending:Boolean = false;
		public var deviceList:ArrayCollection;
		
		public function Devices()
		{
			this.dispatcher = new EventDispatcher(this);
			
			this.deviceList = new ArrayCollection();
			
			this.getDevices(false);
			
			this.deviceSearchTimer = new Timer(1000);
			this.deviceSearchTimer.addEventListener(TimerEvent.TIMER, function():void { getDevices(true); });
			this.deviceSearchTimer.start();
		}
		
		private function getDevices(async:Boolean = false):void
		{
			var file:File = new File(MediaConstants.QNET_BASE_DIR);

			if(async && !this.isAsyncDirOperationPending)
			{
				var handleDirListing:Function = function(event:FileListEvent):void
				{
					checkForDevices(event.files);
					file.removeEventListener(FileListEvent.DIRECTORY_LISTING, handleDirListing);
				};

				file.addEventListener(FileListEvent.DIRECTORY_LISTING, handleDirListing);
				
				// Get the list of mounts in the qnet dir
				this.isAsyncDirOperationPending = true;
				file.getDirectoryListingAsync();
			}
			else
			{
				this.checkForDevices(file.getDirectoryListing());
			}
		}
		
		private function checkForDevices(dirList:Array):void
		{
			var foundDevices:Array = new Array();
			
			for each(var dir:File in dirList)
			{
				if(isMediaStore(dir))
				{
					// Get the 'friendly name' for each of the shares.
					// We assume that the qnet directory representing each path do NOT
					// have any similarities that will cause the following conditional statements
					// to be true for more than one of the qnet mounted directories
					// default to net name in case friendly name not found
					var qnetName:String = "";
					var name:String = dir.name;
					if(dir.name.toLowerCase().indexOf(DEVICE_HEAD_NAME) != -1)
					{
						qnetName = DEVICE_HEAD_NAME;
						name = DEVICE_HEAD_FRIENDLY_NAME;
					}
					else if(dir.name.toLowerCase().indexOf(DEVICE_LEFT_REAR_NAME) != -1)
					{
						qnetName = DEVICE_LEFT_REAR_NAME;
						name = DEVICE_LEFT_REAR_FRIENDLY_NAME;
					}
					else if(dir.name.toLowerCase().indexOf(DEVICE_RIGHT_REAR_NAME) != -1)
					{
						qnetName = DEVICE_RIGHT_REAR_NAME;
						name = DEVICE_RIGHT_REAR_FRIENDLY_NAME;
					}
					
					// Check if this device already exists in the device list
					var existingDevice:Device = getDeviceByQnetName(qnetName);
					
					if(existingDevice == null)
					{
						var device:Device = new Device(dir.nativePath, qnetName, name)
						
						// Propogate the context name changed event to any listeners we have to the Devices instance
						device.addEventListener(DeviceEvent.CONTEXT_NAME_UPDATED, dispatchEvent);
						
						deviceList.addItem(device);
						
						// Add this to the list of found devices
						foundDevices.push(device);
						
						// Send an event notifying that a device was added
						dispatchEvent(new DeviceEvent(DeviceEvent.ADDED, device));
						trace("Device added: " + device.qnetName);
					}
					else
					{
						// Add this to the list of found devices
						foundDevices.push(existingDevice);
					}
				}
			}
			
			// Remove the missing devices
			removeMissingDevices(foundDevices);
			
			if(this.isAsyncDirOperationPending)
			{
				this.isAsyncDirOperationPending = false;
			}
		}
		
		private function removeMissingDevices(foundDevices:Array):void
		{
			// We now need to check if there were any devices which we have in our list
			// which WEREN'T found
			var devicesToRemove:Array = new Array();
			
			for each(var existingDevice:Device in this.deviceList)
			{
				var existingDeviceFound:Boolean = false;
				for each(var foundDevice:Device in foundDevices)
				{
					if(existingDevice.qnetName == foundDevice.qnetName)
					{
						existingDeviceFound = true;
						break;
					}
				}
				
				if(!existingDeviceFound)
				{
					devicesToRemove.push(existingDevice);
				}
			}
			
			// So we don't affect the collection as we iterate through it..
			for(var i:uint=0; i < devicesToRemove.length; i++)
			{
				(devicesToRemove[i] as Device).dispose();
				
				this.deviceList.removeItemAt(this.deviceList.getItemIndex(devicesToRemove[i]));
				trace("Device removed: " + devicesToRemove[i].qnetName);
				
				// Dispatch an event
				this.dispatchEvent(new DeviceEvent(DeviceEvent.REMOVED, devicesToRemove[i]));
			}
		}
		
		public function getDeviceByQnetName(qnetName:String):Device
		{
			var result:Device = null;
			for each(var device:Device in deviceList)
			{
				if(device.qnetName == qnetName)
				{
					result = device;
					break;
				}
			}
			return result;
		}
		
		public function getDeviceByMediaPlayerContextName(name:String):Device
		{
			var result:Device;
			for each(var device:Device in deviceList)
			{
				if(device.mediaPlayerContextName == name)
				{
					result = device;
					break;
				}
			}
			return result;
		}
		
		private function isMediaStore(dir:File):Boolean
		{
			var file:File = new File(dir.nativePath + MediaConstants.MEDIA_LIBRARY_PATH);
			return file.exists;
		}
		
		/* Required functions to implement IEventDispatcher */
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			this.dispatcher.addEventListener(type, listener, useCapture, priority);
		}
		
		public function dispatchEvent(evt:Event):Boolean
		{
			return this.dispatcher.dispatchEvent(evt);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return this.dispatcher.hasEventListener(type);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			this.dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function willTrigger(type:String):Boolean
		{
			return this.dispatcher.willTrigger(type);
		}

	}
}