package com.lixar.components.mediaplayer
{
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	public class Playlist
	{
		public var tracks:Vector.<MediaFile>;
		public var name:String;
		
		public function Playlist()
		{
			tracks = new Vector.<MediaFile>;
		}
		
		public function save(path:String):void
		{
			var file:File = new File(path);
			if(file.exists)
			{
				file.deleteFile();
			}
			var stream:FileStream = new FileStream();
			var playlistData:String = "";
			for each(var track:MediaFile in tracks)
			{
				playlistData += "file://" + track.nativePath + "\n";
			}
			stream.open(file, FileMode.WRITE);
			stream.writeUTFBytes(playlistData);
			stream.close();
		}
	}
}