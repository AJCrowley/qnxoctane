package com.lixar.components.mediaplayer
{
	import com.lixar.qnx.octane.mediaplayer.events.DeviceEvent;
	import com.lixar.util.Singleton;
	
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.collections.ArrayCollection;
	
	import qnx.events.MediaPlayerEvent;
	import qnx.events.MediaServiceConnectionEvent;
	import qnx.events.MediaServiceRequestEvent;
	import qnx.events.PPSEvent;
	import qnx.media.MediaPlayerState;
	import qnx.media.MediaServiceConnection;
	import qnx.media.VideoDisplay;
	import qnx.pps.PPS;
	import qnx.system.AudioManager;

	[Bindable]
	public class MediaController extends Singleton implements IEventDispatcher
	{
		/**
		 * Threshold in milliseconds that tells the media controller to move to the next
		 * track if the media player's isPlaying property changes to <strong>false</strong>. 
		 */		
		private static const NEXT_TRACK_ON_STOP_THRESHOLD:uint = 2000;
		
		public var mediaPlayer:MediaPlayer;
		
		/**
		 * Stores the instance of the media controller PPS server control object 
		 */		
		private var _mediaControllerService:MediaControllerService;
		
		/**
		 * AudioManager instance that will give us access to change the local device's
		 * volume level.
		 */		
		private var audioManager:AudioManager = AudioManager.audioManager;
		
		public var currentMediaFile:MediaFile;
		
		public var shuffle:Boolean = false;
		public var repeat:int = 0;
		public var currentPlaylist:Playlist;
		public var currentTrack:uint = 0;
		public var mediaService:MediaServiceConnection;
				
		public var videoResumePosition:uint = 0;
		
		private var dispatcher:EventDispatcher;
		
		private var _devices:Devices;
		
		/**
		 * Stores an object reference to the Device object for the <strong>local</strong> device;
		 * i.e. the device for which the MediaController.mediaPlayer is controlling. 
		 */
		public var localDevice:Device;
		
		/**
		 * Stores an object reference to the Device which all media commands should
		 * be sent to. If the target device is the same as the local device, then commands
		 * are sent through the MediaPlayer instance. If the target device is a remote
		 * device, then RemoteCommand objects will be instantiated and used to send
		 * PPS commands to the remote devices to control their MediaPlayer instances.
		 */
		private var _targetDevice:Device;
		
		public function MediaController()
		{
			dispatcher = new EventDispatcher(this);
						
			// Create the media controller service
			this._mediaControllerService = new MediaControllerService(this);
			
			// create media service connection to respond to external events
			mediaService = new MediaServiceConnection();
			mediaService.addEventListener
			(
				MediaServiceConnectionEvent.CONNECT,
				function(event:MediaServiceConnectionEvent):void
				{
					// set up handlers for MediaService events
					mediaService.addEventListener(MediaServiceRequestEvent.TRACK_PLAY, function():void { togglePause(true); });
					mediaService.addEventListener(MediaServiceRequestEvent.TRACK_PAUSE, function():void { togglePause(true); });
					mediaService.addEventListener(MediaServiceRequestEvent.TRACK_NEXT, function():void { next(true); });
					mediaService.addEventListener(MediaServiceRequestEvent.TRACK_PREV, function():void { prev(true); });
				}
			);
			
			mediaService.connect();
			
			// Initialize the media player
			this.initializeMediaPlayer();

			// Initialize the local and target devices to THIS device
			// Attempt to get the device from the devices instance
			this._devices = Singleton.getInstance(Devices) as Devices;
			this.localDevice = this.targetDevice = this._devices.getDeviceByMediaPlayerContextName(this.mediaPlayer.contextName);
			if(this.localDevice == null)
			{
				var deviceContextNameUpdated:Function = function(event:DeviceEvent):void {
					// Attempt to find the device again
					if(event.device.mediaPlayerContextName == mediaPlayer.contextName)
					{
						localDevice = targetDevice = event.device;
						_devices.removeEventListener(DeviceEvent.CONTEXT_NAME_UPDATED, deviceContextNameUpdated);
						dispatchEvent(new Event(MediaConstants.EVENT_LOCAL_DEVICE_UPDATED));
						dispatchEvent(new Event(MediaConstants.EVENT_TARGET_DEVICE_UPDATED));
					}
				};
				
				// If the device wasn't found, it's possible that context name simply hasn't been loaded from the PPS file yet
				this._devices.addEventListener(DeviceEvent.CONTEXT_NAME_UPDATED, deviceContextNameUpdated);
			}
			else
			{
				// We didn't have to wait to get a ref to the local device, so we can send the events right away
				this.dispatchEvent(new Event(MediaConstants.EVENT_LOCAL_DEVICE_UPDATED));
				this.dispatchEvent(new Event(MediaConstants.EVENT_TARGET_DEVICE_UPDATED));
			}
		}
		
		private function initializeMediaPlayer():void
		{
			try
			{
				// try to hook into multimedia renderer
				this.mediaPlayer = new MediaPlayer();
			}
			catch(error:Error)
			{
				// couldn't hook into MM-Renderer
				throw(error);
				// TODO: This error is fatal since we can't continue with the application
			}
			
			// uncomment for media player debug output to console
			/*mediaPlayer.debugOut = function(msg:String):void
			{
			trace(msg);
			};*/
			
			this.mediaPlayer.addEventListener
			(
				MediaPlayerEvent.PREPARE_COMPLETE,
				function(event:MediaPlayerEvent):void 
				{
					// prepare complete, start playing audio
					mediaPlayer.play();
					
					// let media service know that we're playing
					mediaService.setPlayState(MediaPlayerState.PLAYING);
					// check now playing pps exists
					var file:File = new File(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE);
					if(!file.exists)
					{
						var stream:FileStream = new FileStream();
						stream.open(file, FileMode.WRITE);
						stream.close();
					}
					// set now playing pps
					var pps:PPS = new PPS();
					pps.addEventListener
					(
						PPSEvent.PPS_OPENED,
						function(event:PPSEvent):void
						{
							var nowPlaying:NowPlaying;
							if(currentMediaFile is AudioFile)
							{
								nowPlaying = new NowPlaying
								(
									(currentMediaFile as AudioFile).title,
									(currentMediaFile as AudioFile).artist,
									(currentMediaFile as AudioFile).album,
									(currentMediaFile as AudioFile).duration,
									(currentMediaFile as AudioFile).artwork,
									MediaConstants.MEDIA_TYPE_AUDIO
								);
							}
							else if(currentMediaFile is VideoFile)
							{
								nowPlaying = new NowPlaying
								(
									(currentMediaFile as VideoFile).title,
									null,
									null,
									(currentMediaFile as VideoFile).duration,
									null,
									MediaConstants.MEDIA_TYPE_VIDEO
								);
							}
							else
							{
								nowPlaying = new NowPlaying
								(
									currentMediaFile.title,
									null,
									null,
									currentMediaFile.duration,
									null,
									MediaConstants.MEDIA_TYPE_NONE
								);
							}
							pps.write
							(
								{
									"nowPlaying":
									{
										"title": nowPlaying.title,
										"artist": nowPlaying.artist,
										"album": nowPlaying.album,
										"duration": nowPlaying.duration,
										"art": nowPlaying.art,
										"mediaType": nowPlaying.mediaType
									}
								}
							);
							pps.close();
						}
					);
					pps.openAsync(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE, PPS.O_WRONLY);
				}
			);
			
			this.mediaPlayer.addEventListener
			(
				ErrorEvent.ERROR, 
				function(event:Event=null):void
				{
					// handle error
					trace(event.toString());
					trace("MediaPlayer Error Code: " + mediaPlayer.getErrorCode().toString());
				}
			);
			
			this.mediaPlayer.addEventListener
			(
				MediaPlayerEvent.INFO_CHANGE, 
				function(event:MediaPlayerEvent):void
				{
					// listen for state change events
					if(event.what.state)
					{
						// not playing, track is over
						if(!mediaPlayer.isPlaying && mediaPlayer.position >= (mediaPlayer.duration - NEXT_TRACK_ON_STOP_THRESHOLD))
						{
							// store current device context
							var tempDevice:Device = _targetDevice
							// switch context to local
							_targetDevice = this.localDevice;
							// so play the next one
							next(true);
							// and switch back, phew
							_targetDevice = tempDevice;
						}
					}
				}
			);
		}
		
		private function disposeMediaPlayer():void
		{
			this.mediaPlayer.dispose();
			this.mediaPlayer = null;
		}
		
		public function resetNowPlayingPPS(event:Event = null):void
		{
			var file:File = new File(MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE);
			if(file.exists)
			{
				file.deleteFileAsync();
			}
		}

		/*
		Getter & Setter for targetDevice so we can always have one defined
		*/
		public function get targetDevice():Device
		{
			if (this._targetDevice == null)
			{
				// FIXME: Since the Device's media player context name value is loaded asynchronously,
				// we don't have a way to initialize the MediaController with that information present
				// so we're doing a check here FOR THE TIME BEING that will set the local device and
				// target device is neither is set. Although this isn't guaranteed to work since
				// the media player context names for the Devices may not have been loaded yet,
				// it will be good enough for demo purposes.
				if (this.localDevice == null)
				{
					this.localDevice = (Singleton.getInstance(Devices) as Devices).getDeviceByMediaPlayerContextName(this.mediaPlayer.contextName);
				}
				
				this._targetDevice = this.localDevice;
			}
			return this._targetDevice;
		}

		public function set targetDevice(dev:Device):void
		{
			this._targetDevice = dev;
			this.dispatchEvent(new Event(MediaConstants.EVENT_TARGET_DEVICE_UPDATED));
		}
		
		public function get videoDisplay():VideoDisplay
		{
			return mediaPlayer.videoDisplay;
		}
		
		public function set videoDisplay(display:VideoDisplay):void
		{
			// Every time the video display changes, we need to reinitialize the media player
			if(display != null)
			{
				this.disposeMediaPlayer();
				this.initializeMediaPlayer();
			}
			
			mediaPlayer.videoDisplay = (display ? display : new VideoDisplay());
		}
		
		/**
		 * Helper method to determine if the MediaController's targetDevice is the same as the localDevice.
		 * 
		 * @return Boolean <strong>TRUE</strong> if the target device is the same as the local device, 
		 * <strong>False</strong> if not
		 * 
		 */		
		public function isTargetDeviceLocal():Boolean
		{
			var isTargetLocal:Boolean = false;
			if(this.targetDevice != null && this.localDevice != null &&
				this.targetDevice == this.localDevice)
			{
				isTargetLocal = true;
			}
			return isTargetLocal;
		}
		
		public function play(playlist:Playlist = null, trackIndex:int = -1, forceLocal:Boolean = false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				if(playlist)
				{
					currentPlaylist = playlist;
				}
				if(trackIndex != -1)
				{
					currentTrack = trackIndex;
				}
				// get current file from playlist
				currentMediaFile = currentPlaylist.tracks[currentTrack];
				// plug url into mediaplayer
				mediaPlayer.url = "file://" + currentMediaFile.nativePath;
				// and prepare to play
				mediaPlayer.prepare();
			}
			else
			{
				// The play remote command should be an invoke command if at least one of the following conditions are true:
				// 1) The target device is NOT currently running the media player
				// 2) The media type of what we're trying to play is Video (i.e. a video should always be forced to the foreground)
				var command:RemoteCommand = new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_PLAY);
				
				// Check if the player is already running
				if(!this.targetDevice.isMediaPlayerRunning)
				{
					command.isInvoke = true;
				}
				
				var trackList:String = "";
				for(var i:uint = 0; i < playlist.tracks.length; i++)
				{
					trackList += playlist.tracks[i].id + MediaConstants.REMOTE_COMMAND_LIST_DELIMITER;
				}
				trackList = trackList.slice(0, -1);
				var mediaLibrary:MediaLibrary = Singleton.getInstance(MediaLibrary) as MediaLibrary;
				
				// Create the arguments for the remote command
				var args:Array = new Array();
				
				// The source device for the media. This is the 'qnet name' constant we have for the device
				args[MediaConstants.REMOTE_COMMAND_PLAY_SOURCE_DEVICE_QNET_NAME] = mediaLibrary.device.qnetName;

				// The media type of the media we want to play, since we don't have that info from just file IDs
				// TODO: Who's to say that every media file in the playlist is the same type as the first?
				var mediaType:String = "";
				if(playlist.tracks.length > 0)
				{
					if(playlist.tracks[0] is AudioFile)
					{
						mediaType = MediaConstants.MEDIA_TYPE_AUDIO;
					}
					else if(playlist.tracks[0] is VideoFile)
					{
						mediaType = MediaConstants.MEDIA_TYPE_VIDEO;
						
						// We need to force the remote command to be an invoke
						command.isInvoke = true;
					}
					else
					{
						// This shouldn't really happen..
						mediaType = MediaConstants.MEDIA_TYPE_NONE;
					}
				}
				args[MediaConstants.REMOTE_COMMAND_PLAY_MEDIA_TYPE] = mediaType;
				
				// The delimited list of file IDs
				args[MediaConstants.REMOTE_COMMAND_PLAY_TRACK_LIST] = trackList;
				
				// The index within the list of file IDs which should be the first track to play
				args[MediaConstants.REMOTE_COMMAND_PLAY_CURRENT_TRACK] = trackIndex;

				command.args = args;
				command.sendCommand();
			}
		}
		
		public function playAlbum(sourceDevice:Device, albumId:uint, forceLocal:Boolean = false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				var mediaLibrary:MediaLibrary = Singleton.getInstance(MediaLibrary) as MediaLibrary;
				mediaLibrary.init(sourceDevice);
				
				var playlist:Playlist = new Playlist();
				
				// Get tracks by artist ID
				var tracks:ArrayCollection = mediaLibrary.getMusicByAlbum(albumId);
				
				for each(var track:AudioFile in tracks)
				{
					playlist.tracks.push(track);
				}
				
				// Start playing
				this.play(playlist, 0, forceLocal);
			}
			else
			{
				var remoteCommand:RemoteCommand = new RemoteCommand(this.targetDevice, MediaConstants.REMOTE_COMMAND_PLAY_ALBUM);
				if(!this.targetDevice.isMediaPlayerRunning)
				{
					remoteCommand.isInvoke = true;
				}
				remoteCommand.args = new Array(sourceDevice.qnetName, albumId);
				remoteCommand.sendCommand();
			}
		}
		
		public function playArtist(sourceDevice:Device, artistId:uint, forceLocal:Boolean = false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				var mediaLibrary:MediaLibrary = Singleton.getInstance(MediaLibrary) as MediaLibrary;
				mediaLibrary.init(sourceDevice);
				
				var playlist:Playlist = new Playlist();
				
				// Get tracks by artist ID
				var tracks:ArrayCollection = mediaLibrary.getMusicByArtist(artistId);
				
				for each(var track:AudioFile in tracks)
				{
					playlist.tracks.push(track);
				}
				
				// Start playing
				this.play(playlist, 0, forceLocal);
			}
			else
			{
				var remoteCommand:RemoteCommand = new RemoteCommand(this.targetDevice, MediaConstants.REMOTE_COMMAND_PLAY_ARTIST);
				if(!this.targetDevice.isMediaPlayerRunning)
				{
					remoteCommand.isInvoke = true;
				}
				remoteCommand.args = new Array(sourceDevice.qnetName, artistId);
				remoteCommand.sendCommand();
			}
		}

		public function stop(forceLocal:Boolean=false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				mediaPlayer.stop();
				
				// Clear now playing info since we're not 'playing' anything
				this.resetNowPlayingPPS();
			}
			else
			{
				new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_STOP, true);
			}
		}
		
		public function togglePause(forceLocal:Boolean=false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				// toggle pause state, update media service
				if(mediaPlayer.isPlaying)
				{
					if(mediaPlayer.isPaused)
					{
						mediaPlayer.play();
						mediaService.setPlayState(MediaPlayerState.PLAYING);
					}
					else
					{
						mediaPlayer.pause();
						mediaService.setPlayState(MediaPlayerState.PAUSED);
					}
					this.dispatchEvent(new Event(MediaConstants.EVENT_TOGGLE_PAUSE));
				}
			}
			else
			{
				new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_PAUSE, true);
			}
		}
		
		public function next(forceLocal:Boolean=false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				var next:int = -1;
				if(shuffle)
				{
					next = currentTrack;
					while(next == currentTrack)
					{
						next = Math.round(Math.random() * (currentPlaylist.tracks.length - 1));
					}
				}
				else if (repeat == 2)
				{
					next = currentTrack;
				}
				else
				{
					if((currentPlaylist.tracks.length - 1 > currentTrack))
					{
						next = currentTrack + 1;
					}
					else if(repeat == 1)
					{
						next = 0;
					}
				}
				if(next != -1)
				{
					currentTrack = next;
					play(null, -1, true);
				}
				else
				{
					this.stop(true);
				}
			}
			else
			{
				new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_NEXT, true);
			}
		}
		
		public function prev(forceLocal:Boolean=false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				if(currentTrack > 0)
				{
					currentTrack--;
					play(null, -1, true);
				}
			}
			else
			{
				new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_PREV, true);
			}
		}
		
		public function seek(position:uint, forceLocal:Boolean=false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				this.mediaPlayer.seek(position);
			}
			else
			{
				// Remote command
				var command:RemoteCommand = new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_SEEK);
				var args:Array = new Array();
				args[MediaConstants.REMOTE_COMMAND_SEEK_POSITION] = position;
				command.args = args;
				command.sendCommand();
			}
		}
		
		/**
		 * Sets the volume level of the target device.
		 * @param level Number A level value between 0 and 100, 0 being no volume, and 100 being max volume.
		 * @param forceLocal Boolean Specifies whether the volume level should be forced to be set
		 * 			on the local device, or if it can be sent to its target device via remote command.
		 * 
		 */		
		public function setVolumeLevel(level:Number, forceLocal:Boolean = false):void
		{
			if(this.isTargetDeviceLocal() || forceLocal)
			{
				this.audioManager.setOutputLevel(level <= 100 ? level : 100);
			}
			else
			{
				// Remote command
				var command:RemoteCommand = new RemoteCommand(targetDevice, MediaConstants.REMOTE_COMMAND_SET_VOLUME);
				var args:Array = new Array();
				args[MediaConstants.REMOTE_COMMAND_SET_VOLUME_LEVEL] = level;
				command.args = args;
				command.sendCommand();
			}
		}
		
		public function disconnectControlService():void
		{
			this._mediaControllerService.disconnect();
		}
		
		/* Required functions to implement IEventDispatcher */
		public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void
		{
			dispatcher.addEventListener(type, listener, useCapture, priority);
		}
		
		public function dispatchEvent(evt:Event):Boolean
		{
			return dispatcher.dispatchEvent(evt);
		}
		
		public function hasEventListener(type:String):Boolean
		{
			return dispatcher.hasEventListener(type);
		}
		
		public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void
		{
			dispatcher.removeEventListener(type, listener, useCapture);
		}
		
		public function willTrigger(type:String):Boolean
		{
			return dispatcher.willTrigger(type);
		}
	}
}