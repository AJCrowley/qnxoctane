package com.lixar.components.mediaplayer
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import qnx.events.PPSChannelEvent;
	import qnx.events.PPSEvent;
	import qnx.pps.Message;
	import qnx.pps.PPSChannel;

	public class RemoteCommand extends EventDispatcher
	{
		private var _device:Device;
		private var ppsInvokeChannel:PPSChannel;
		
		public var isInvoke:Boolean = false;
		public var command:String;
		public var args:Array;
		
		public function RemoteCommand(device:Device, command:String = null, sendNow:Boolean = false, isInvoke:Boolean = false)
		{
			_device = device;
			
			if(command)
			{
				this.command = command;
			}
			
			if(sendNow)
			{
				this.sendCommand();
			}
			
			this.isInvoke = isInvoke;
		}
		
		public function sendCommand():void
		{
			if(this.isInvoke)
			{
				// This is a navigator invoke command
				this.ppsInvokeChannel = new PPSChannel();
				this.ppsInvokeChannel.addEventListener(PPSChannelEvent.OPEN, this.invokeChannelConnected);
				this.ppsInvokeChannel.connect(this._device.basePath + MediaConstants.INVOKE_COMMAND_PPS_PATH, false);
			}
			else
			{
				// The remote command should be sent to the Media Player PPS control object instead
				// We can do this through the device's control object
				if(this._device.isMediaPlayerRunning)
				{
					this._device.ppsController.sendMsg(new Message(this.command, this.args ? this.args.join(MediaConstants.REMOTE_COMMAND_DELIMITER) : null));
				}
			}
		}
		
		private function invokeChannelConnected(event:PPSChannelEvent):void
		{
			var ppsMessage:Message;
			var msgData:String = "";
			
			// Because this is an invoke command, the message data is the invoke URL associated
			// with the media player app, followed by the command string, finally followed by zero
			// or more arguments
			msgData += MediaConstants.INVOKE_URL_PROTOCOL + this.command;

			// Append the args to the message data if they exist
			if(args)
			{
				msgData += MediaConstants.REMOTE_COMMAND_DELIMITER + this.args.join(MediaConstants.REMOTE_COMMAND_DELIMITER);
			}
			
			ppsMessage = new Message(MediaConstants.INVOKE_COMMAND, msgData);

			// Send the invoke command
			this.ppsInvokeChannel.sendMsg(ppsMessage);
			
			// Disconnect
			this.ppsInvokeChannel.disconnect();
			
			// Clean up the event listener
			this.ppsInvokeChannel.removeEventListener(PPSChannelEvent.OPEN, this.invokeChannelConnected);
		}
	}
}