package com.lixar.components.mediaplayer
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.filesystem.File;
	
	import spark.components.Image;

	[Bindable]
	public class MediaFile
	{
		public var id:uint;
		public var title:String = "Unknown";
		public var file:File;
		public var duration:uint;
		public var artwork:String = "resources/album_cover_blank_small.png";
		public var thumbnail:Image = new Image();
		public var thumbId:uint;
		
		public function get nativePath():String
		{
			return file.nativePath;
		}
		
		public function MediaFile()
		{
		}
		
		/*public function get thumbnail():Bitmap {
			return new Bitmap(new BitmapData(200, 200));
		}*/
	}
}