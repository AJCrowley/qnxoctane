package com.lixar.components.mediaplayer
{
	import spark.components.Image;

	public class MediaThumbnail
	{
		public var thumbId:uint;
		public var deviceQnetName:String;
		public var thumbnail:Image;
		
		public function MediaThumbnail()
		{
		}
	}
}