package com.lixar.components.mediaplayer
{
	import flash.display.Bitmap;
	import flash.filesystem.File;

	[Bindable]
	public class AudioFile extends MediaFile
	{
		public var artist:String = "Unknown Artist";
		public var album:String = "Unknown Album";
		public var track:uint;
		
		public function AudioFile()
		{
			super();
			artwork = "resources/album_cover_blank_small.png";
		}		
	}
}