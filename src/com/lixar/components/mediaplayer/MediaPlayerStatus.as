package com.lixar.components.mediaplayer
{
	[Bindable]
	public class MediaPlayerStatus
	{
		public var isPlaying:Boolean = false;
		public var position:uint = 0;
		
		public function MediaPlayerStatus()
		{
		}
	}
}