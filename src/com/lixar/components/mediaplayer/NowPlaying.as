package com.lixar.components.mediaplayer
{
	[Bindable]
	public class NowPlaying
	{
		public var title:String = "Unknown Title";
		public var artist:String = "";
		public var album:String = "";
		public var art:String;
		public var duration:uint = 0;
		public var mediaType:String = MediaConstants.MEDIA_TYPE_NONE;
		
		public function NowPlaying(title:String = null, artist:String = null, album:String = null, duration:uint = 0, art:String = null, mediaType:String = null)
		{
			if(title)
			{
				this.title = title;
			}
			if(artist)
			{
				this.artist = artist;
			}
			if(album)
			{
				this.album = album;
			}
			if(art)
			{
				this.art = art;
			}
			
			this.duration = duration;
			
			if(mediaType)
			{
				this.mediaType = mediaType;
			}
		}
	}
}