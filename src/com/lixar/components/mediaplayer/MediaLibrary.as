package com.lixar.components.mediaplayer
{
	import com.lixar.util.Singleton;
	
	import flash.data.SQLConnection;
	import flash.data.SQLMode;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.display.Bitmap;
	import flash.filesystem.File;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.core.BitmapAsset;
	import mx.states.AddItems;
	
	import spark.components.Image;

	public class MediaLibrary extends Singleton
	{
		private static const ANDROID_MEDIA_PATH:String = "/misc/android/";
		private static const ALBUM_AGGREGATE_INFO_RELATION:String = "(SELECT album_id, COUNT(*) totalTracks, SUM(duration) AS totalTrackDuration " +
																"FROM audio_metadata  " +
																"INNER JOIN files ON audio_metadata.fid = files.fid  " +
																"INNER JOIN folders ON files.folderid = folders.folderid  " +
																"AND folders.basepath NOT LIKE  '" + ANDROID_MEDIA_PATH + "%' " +
																"GROUP BY album_id)";
		private static const ARTIST_AGGREGATE_INFO_RELATION:String = "(SELECT artist_id, COUNT(*) totalTracks, COUNT(DISTINCT album_id) AS totalAlbums, SUM(duration) AS totalTrackDuration " +
																"FROM audio_metadata  " +
																"INNER JOIN files ON audio_metadata.fid = files.fid  " +
																"INNER JOIN folders ON files.folderid = folders.folderid  " +
																"AND folders.basepath NOT LIKE  '" + ANDROID_MEDIA_PATH + "%' " +
																"GROUP BY artist_id)";
		
		private var dbFile:File;
		private var dbConn:SQLConnection;
		private var mediaMountPath:String;
				
		private var _device:Device;
		private var _thumbnails:Vector.<MediaThumbnail> = new Vector.<MediaThumbnail>;
		
		public function get device():Device { return this._device; }
		
		public function MediaLibrary()
		{
			
		}
		
		public function init(device:Device):void
		{
			this._device = device;
			
			dbFile = new File(this._device.basePath + MediaConstants.MEDIA_LIBRARY_PATH);
			// Since it's possible that this is a RE-init of the MediaLibrary, we should close any existing
			// database connections since they can possibly be connections to a different database than 
			// what we want.
			if(this.dbConn && this.dbConn.connected)
			{
				this.closeDatabase();
			}
			
			// Now we can re-open the database
			openDatabase();
			
			// Get the media mount path right away since we need it almost everywhere
			var sqlStatement:SQLStatement = getSqlStatement("SELECT mediastore_metadata.mountpath FROM mediastore_metadata");
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();

			// There should only be one result here
			if(result.data.length != 1)
			{
				throw new Error("Can't initialize MediaLibrary.mediaMountPath");
			}
			mediaMountPath = this._device.basePath + result.data[0].mountpath;
		}
		
		public function getMusicAlbums():ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement("SELECT DISTINCT albums.album_id, albums.album, artists.artist_id, artists.artist, audioAggregateInfo.totalTracks, audioAggregateInfo.totalTrackDuration, photopath, audio_artworks.thumbid " +
				"FROM albums " +
				"INNER JOIN audio_metadata ON albums.album_id = audio_metadata.album_id " +
				"INNER JOIN artists ON audio_metadata.artist_id = artists.artist_id " +
				"LEFT JOIN audio_artworks ON audio_artworks.artwork_id = audio_metadata.artwork_id " +
				"INNER JOIN files ON audio_metadata.fid = files.fid " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"	AND folders.basepath NOT LIKE '" + ANDROID_MEDIA_PATH + "%' " + 
				"INNER JOIN " + ALBUM_AGGREGATE_INFO_RELATION + " AS audioAggregateInfo ON audio_metadata.album_id = audioAggregateInfo.album_id " +
				"ORDER BY album");
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			
			var albums:ArrayCollection = new ArrayCollection();
			var trackList:ArrayCollection;
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var album:Album = new Album();
				album.id = row.album_id;
				album.name = row.album;
				
				var artist:Artist = new Artist();
				artist.id = row.artist_id;
				artist.name = row.artist;
				album.artist = artist;
				album.totalTracks = row.totalTracks;
				album.totalTrackDuration = row.totalTrackDuration;
				if(row.thumbid)
				{
					album.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				albums.addItem(album);
			}
			loadThumbs(thumbnails);
			for each(album in albums)
			{
				album.thumbnail = getThumbnail(album.thumbId);
			}
			return albums;
		}
		
		public function getMusicArtists():ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement("SELECT DISTINCT artists.*, audio_artworks.thumbid, audioAggregateInfo.totalAlbums, audioAggregateInfo.totalTracks, audioAggregateInfo.totalTrackDuration " +
				"FROM artists " +
				"LEFT JOIN audio_artworks ON artists.artist_id = audio_artworks.artist_id " +
				"	AND audio_artworks.thumbid <= (SELECT MIN(audio_artworks.thumbid) FROM audio_artworks WHERE audio_artworks.artist_id = artists.artist_id) " +
				"INNER JOIN " + ARTIST_AGGREGATE_INFO_RELATION + " AS audioAggregateInfo ON artists.artist_id = audioAggregateInfo.artist_id " +
				"ORDER BY artists.artist");
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();

			var artists:ArrayCollection = new ArrayCollection();
			var trackList:ArrayCollection;
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var artist:Artist = new Artist();
				artist.id = row.artist_id;
				artist.name = row.artist;
				if(row.thumbid)
				{
					artist.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				artist.totalAlbums = row.totalAlbums;
				artist.totalTracks = row.totalTracks;
				artist.totalTrackDuration = row.totalTrackDuration;
				
				artists.addItem(artist);
			}
			loadThumbs(thumbnails);
			for each(artist in artists)
			{
				artist.thumbnail = getThumbnail(artist.thumbId);
			}
			return artists;

			//return new ArrayCollection(result.data);
		}
		
		public function getAllMusic():ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement(
				"SELECT DISTINCT audio_metadata.fid, audio_metadata.title, artists.artist, albums.album, audio_metadata.track, audio_metadata.duration, folders.basepath, files.filename, audio_artworks.photopath, audio_artworks.thumbid " +
				"FROM audio_metadata " +
				"INNER JOIN files ON audio_metadata.fid = files.fid " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"	AND folders.basepath NOT LIKE '" + ANDROID_MEDIA_PATH + "%' " + 
				"INNER JOIN artists ON audio_metadata.artist_id = artists.artist_id " +
				"INNER JOIN albums ON audio_metadata.album_id = albums.album_id " +
				"LEFT JOIN audio_artworks ON audio_metadata.artwork_id = audio_artworks.artwork_id " +
				"GROUP BY audio_metadata.fid ORDER BY title");
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			
			var music:ArrayCollection = new ArrayCollection();
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var audio:AudioFile = new AudioFile();
				audio.id = row.fid;
				audio.title = row.title;
				audio.artist = row.artist;
				audio.album = row.album;
				audio.track = row.track;
				if(row.photopath != null)
				{
					audio.artwork = "file://" + mediaMountPath + row.photopath;
				}
				//audio.artwork = (row.photopath != null ? "file://" + mediaMountPath + row.photopath : "");
				if(row.thumbid)
				{
					audio.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				audio.duration = row.duration;
				audio.file = new File(this.mediaMountPath + row.basepath + row.filename);	
				music.addItem(audio);
			}
			loadThumbs(thumbnails);
			for each(audio in music)
			{
				audio.thumbnail = getThumbnail(audio.thumbId);
			}
			return music;
		}
		
		public function getMusicByAlbum(albumID:int):ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement(
				"SELECT DISTINCT audio_metadata.fid, audio_metadata.title, artists.artist, albums.album, audio_metadata.track, audio_metadata.duration, folders.basepath, files.filename, audio_artworks.photopath, audio_artworks.thumbid " +
				"FROM audio_metadata " +
				"INNER JOIN files ON audio_metadata.fid = files.fid " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"	AND folders.basepath NOT LIKE '" + ANDROID_MEDIA_PATH + "%' " + 
				"INNER JOIN artists ON audio_metadata.artist_id = artists.artist_id " +
				"INNER JOIN albums ON audio_metadata.album_id = albums.album_id " +
				"LEFT JOIN audio_artworks ON audio_metadata.artwork_id = audio_artworks.artwork_id " +
				"WHERE audio_metadata.album_id = @album_id GROUP BY audio_metadata.fid ORDER BY track");
			sqlStatement.parameters["@album_id"] = albumID;
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
						
			var music:ArrayCollection = new ArrayCollection();
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var audio:AudioFile = new AudioFile();
				audio.id = row.fid;
				audio.title = row.title;
				audio.artist = row.artist;
				audio.album = row.album;
				audio.track = row.track;
				if(row.photopath != null)
				{
					audio.artwork = "file://" + mediaMountPath + row.photopath;
				}
				//audio.artwork = (row.photopath != null ? "file://" + mediaMountPath + row.photopath : "");
				//audio.thumbnail = getThumbnail(row.thumbid);
				if(row.thumbid)
				{
					audio.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				audio.duration = row.duration;
				audio.file = new File(this.mediaMountPath + row.basepath + row.filename);
				
				music.addItem(audio);
			}
			loadThumbs(thumbnails);
			for each(audio in music)
			{
				audio.thumbnail = getThumbnail(audio.thumbId);
			}
			return music;
		}
		
		public function getMusicByArtist(artistID:int):ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement(
				"SELECT DISTINCT audio_metadata.fid, audio_metadata.title, artists.artist, albums.album, audio_metadata.track, audio_metadata.duration, folders.basepath, files.filename, audio_artworks.photopath, audio_artworks.thumbid " +
				"FROM audio_metadata " +
				"INNER JOIN files ON audio_metadata.fid = files.fid " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"	AND folders.basepath NOT LIKE '" + ANDROID_MEDIA_PATH + "%' " + 
				"INNER JOIN artists ON audio_metadata.artist_id = artists.artist_id " +
				"INNER JOIN albums ON audio_metadata.album_id = albums.album_id " +
				"LEFT JOIN audio_artworks ON audio_metadata.artwork_id = audio_artworks.artwork_id " +
				"WHERE audio_metadata.artist_id = @artist_id " +
				"GROUP BY audio_metadata.fid " +
				"ORDER BY albums.album ASC, audio_metadata.track ASC");
			sqlStatement.parameters["@artist_id"] = artistID;
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			
			var music:ArrayCollection = new ArrayCollection();
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var audio:AudioFile = new AudioFile();
				audio.id = row.fid;
				audio.title = row.title;
				audio.artist = row.artist;
				audio.album = row.album;
				audio.track = row.track;
				if(row.photopath != null)
				{
					audio.artwork = "file://" + mediaMountPath + row.photopath;
				}
				//audio.artwork = (row.photopath != null ? "file://" + mediaMountPath + row.photopath : "");
				if(row.thumbid)
				{
					audio.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				audio.duration = row.duration;
				audio.file = new File(this.mediaMountPath + row.basepath + row.filename);
				
				music.addItem(audio);
			}
			loadThumbs(thumbnails);
			for each(audio in music)
			{
				audio.thumbnail = getThumbnail(audio.thumbId);
			}
			return music;
		}
		
		public function getAllVideos():ArrayCollection
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement
			(
				"SELECT video_metadata.fid, video_metadata.title, video_metadata.duration, folders.basepath, files.filename, thumbid " +
				"FROM video_metadata " +
				"INNER JOIN files ON video_metadata.fid = files.fid " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"INNER JOIN files_thumbnails_rel ON files_thumbnails_rel.fid = files.fid " +
				"	AND folders.basepath NOT LIKE '" + ANDROID_MEDIA_PATH + "%' " + 
				"ORDER BY title"
			);
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			
			var videos:ArrayCollection = new ArrayCollection();
			var thumbnails:Array = new Array();
			
			for each(var row:Object in result.data)
			{
				var video:VideoFile = new VideoFile();
				video.id = row.fid;
				video.title = row.title;
				video.duration = row.duration;
				if(row.thumbid)
				{
					video.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				video.file = new File(this.mediaMountPath + row.basepath + row.filename);
				videos.addItem(video);
			}
			loadThumbs(thumbnails);
			for each(video in videos)
			{
				video.thumbnail = getThumbnail(video.thumbId);
			}
			return videos;
		}
		
		public function getPlaylist(trackList:Array):Playlist
		{
			 var playlist:Playlist = new Playlist();
			 var audioFile:AudioFile;
			 openDatabase();
			 var sqlStatement:SQLStatement = getSqlStatement
			(
				"SELECT DISTINCT files.fid, title, artist, album, track, duration, photopath, thumbid, basepath, filename, mountpath " +
					"FROM files, mediastore_metadata " +
					"INNER JOIN folders ON files.folderid = folders.folderid " +
					"INNER JOIN audio_metadata ON audio_metadata.fid = files.fid " +
					"INNER JOIN artists ON audio_metadata.artist_id = artists.artist_id " +
					"INNER JOIN albums ON audio_metadata.album_id = albums.album_id " +
					"LEFT JOIN audio_artworks ON audio_artworks.folderid = files.folderid " +		// LEFT join as there may not be an audio_artowrks record with this condition
					"WHERE files.fid IN (" + trackList.join(",") + ") " +
					"GROUP BY files.fid"
			);
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			var trackArray:Array = new Array();
			var thumbnails:Array = new Array();
			for each(var row:Object in result.data)
			{
				audioFile = new AudioFile();
				audioFile.id = row.fid;
				audioFile.artist = row.artist;
				audioFile.title = row.title;
				audioFile.album = row.album;
				audioFile.track = row.track;
				if(row.photopath != null)
				{
					audio.artwork = "file://" + mediaMountPath + row.photopath;
				}
				//audioFile.artwork = (row.photopath != null ? "file://" + mediaMountPath + row.photopath : "");
				if(row.thumbid)
				{
					audioFile.thumbId = row.thumbid;
					thumbnails.push(row.thumbid);
				}
				audioFile.duration = row.duration;
				audioFile.file = new File(mediaMountPath + row.basepath + row.filename);
				trackArray[trackList.indexOf(String(row.fid))] = audioFile;
			}
			loadThumbs(thumbnails);
			for each(var audio:AudioFile in trackArray)
			{
				audio.thumbnail = getThumbnail(audio.thumbId);
				playlist.tracks.push(audio);
			}
			return playlist;
		}

		public function getVideoFile(fid:int):VideoFile
		{
			var videoFile:VideoFile = new VideoFile();
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement("SELECT DISTINCT title, artist, width, height, duration, basepath, filename, mountpath " +
				"FROM files, mediastore_metadata " +
				"INNER JOIN folders ON files.folderid = folders.folderid " +
				"INNER JOIN video_metadata ON video_metadata.fid = files.fid " +
				"LEFT JOIN artists ON video_metadata.artist_id = artists.artist_id " +
				"WHERE files.fid = @fid");
			sqlStatement.parameters["@fid"] = fid;
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			//closeDatabase();
			if(result)
			{
				videoFile.id = fid;
				videoFile.artist = result.data[0].artist;
				videoFile.title = result.data[0].title;
				videoFile.width = result.data[0].width;
				videoFile.height = result.data[0].height;
				videoFile.duration = result.data[0].duration;
				videoFile.file = new File(mediaMountPath + result.data[0].basepath + result.data[0].filename);
			}
			else
			{
				throw(new Error("File not found"));
			}
			return videoFile;
		}
		
		public function getThumbnail(thumbId:uint):Image
		{
			var thumbImage:Image;
			for each(var thumbnail:MediaThumbnail in _thumbnails)
			{
				if(thumbnail.deviceQnetName == this._device.qnetName &&
					thumbnail.thumbId == thumbId)
				{
					thumbImage = thumbnail.thumbnail;
					break;
				}
			}
			/*if(!thumbImage)
			{
				openDatabase();
				var sqlStatement:SQLStatement = getSqlStatement("SELECT CAST(data AS ByteArray) AS data FROM thumbnails WHERE thumbid = @thumbid");
				sqlStatement.parameters["@thumbid"] = thumbId;
				sqlStatement.execute();
				var result:SQLResult = sqlStatement.getResult();
				closeDatabase();
				var newThumb:MediaThumbnail = new MediaThumbnail();
				newThumb.thumbId = thumbId;
				newThumb.deviceQnetName = this._device.qnetName;
				newThumb.thumbnail = new Image();
				if(result.data != null && result.data[0] != null &&
					result.data[0].data != null && result.data[0].data is ByteArray)
				{
					newThumb.thumbnail.source = result.data[0].data;
				}
				thumbImage = newThumb.thumbnail;
				_thumbnails.push(newThumb);
			}	*/	
			if(!thumbImage)
			{
				thumbImage = new Image();
				thumbImage.source = "resources/album_cover_blank_small.png";
			}
			return thumbImage;
		}
		
		private function loadThumbs(thumbIds:Array):void
		{
			openDatabase();
			var sqlStatement:SQLStatement = getSqlStatement("SELECT thumbid, CAST(data AS ByteArray) AS data FROM thumbnails WHERE thumbid IN (" + thumbIds.join(",") + ")");
			sqlStatement.execute();
			var result:SQLResult = sqlStatement.getResult();
			for each(var row:Object in result.data)
			{
				var newThumb:MediaThumbnail = new MediaThumbnail();
				newThumb.thumbId = row.thumbid;
				newThumb.deviceQnetName = this._device.qnetName;
				newThumb.thumbnail = new Image();
				if(row.data != null && row.data is ByteArray)
				{
					newThumb.thumbnail.source = row.data;
				}
				_thumbnails.push(newThumb);
			}
		}
		
		private function openDatabase():void
		{
			var connect:Boolean = false;
			if(!dbConn)
			{
				connect = true;
			}
			else
			{
				if(!dbConn.connected)
				{
					connect = true;
				}
			}
			if(connect)
			{
				dbConn = new SQLConnection;
				dbConn.open(dbFile, SQLMode.READ);
				if(!dbConn.connected)
				{
					throw(new Error("Unable to open media database"));
				}
			}			
		}
		
		private function closeDatabase():void
		{
			dbConn.close();
		}
		
		private function getSqlStatement(sql:String):SQLStatement
		{
			var sqlStatement:SQLStatement = new SQLStatement();
			sqlStatement.sqlConnection = dbConn;
			sqlStatement.text = sql;
			return sqlStatement;
		}
	}
}