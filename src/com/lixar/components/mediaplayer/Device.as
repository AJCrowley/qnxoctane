package com.lixar.components.mediaplayer
{
	import com.adobe.air.filesystem.FileMonitor;
	import com.adobe.air.filesystem.events.FileMonitorEvent;
	import com.lixar.qnx.octane.mediaplayer.events.DeviceEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.utils.Timer;
	
	import qnx.events.PPSChannelEvent;
	import qnx.events.PPSEvent;
	import qnx.pps.Message;
	import qnx.pps.PPS;
	import qnx.pps.PPSChannel;
	
	[Bindable]
	public class Device extends EventDispatcher
	{
		private static const PPS_CONTROLLER_RECONNECT_DELAY:uint = 1000;
		
		private var _ppsController:PPSChannel;
		public function get ppsController():PPSChannel { return this._ppsController; }
		private var _ppsControllerReconnectTimer:Timer;
		
		public var isMediaPlayerRunning:Boolean = false;
		
		public var basePath:String;
		public var qnetName:String;
		public var name:String;
		public var nowPlaying:NowPlaying;
		public var playerStatus:MediaPlayerStatus;
		public var volumeInfo:DeviceVolumeInfo;
		
		private var _mediaPlayerContextName:String;
		private var devicePPS:PPS;
		private var infoPPS:PPS;
		private var statusPps:PPS;
		private var statePps:PPS;
		
		private var nowPlayingMonitor:FileMonitor;
		private var statusMonitor:FileMonitor;
		private var stateMonitor:FileMonitor;
		
		public function get label():String { return this.name; }
		
		public function get mediaPlayerContextName():String { return this._mediaPlayerContextName; }
		
		public function Device(basePath:String, qnetName:String, name:String)
		{
			this.basePath = basePath;
			this.qnetName = qnetName;
			this.name = name;
			
			// Open a PPS channel to the control object for this device
			this._ppsController = new PPSChannel();
			this._ppsController.addEventListener(PPSChannelEvent.OPEN_FAIL, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.OPEN, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.CONNECT, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.CLOSE, controllerEvent);
			this._ppsController.addEventListener(PPSChannelEvent.DISCONNECT, controllerEvent);
			this.controllerConnect();
			
			// Initialize the volume info
			this.volumeInfo = new DeviceVolumeInfo(this.basePath);
			
			// Initialize the PPS Listener to get info about what's playing on the device
			// Note that this has nothing to do with the device's media player context
			// since we're loading this information from an application-controlled PPS file
			this.initializeNowPlayingListener();
			
			// Initialize the media player status. Values will get filled in once media starts playing on this device
			this.playerStatus = new MediaPlayerStatus();
			
			// Open the PPS file that will give us the 'context' of this media player
			devicePPS = new PPS();
			devicePPS.addEventListener(PPSEvent.PPS_OPENED, setMediaPlayerContext);
			devicePPS.addEventListener(PPSEvent.PPS_CHANGED, setMediaPlayerContext);
			devicePPS.addEventListener(PPSEvent.PPS_REMOVED, setMediaPlayerContext);
			devicePPS.openAsync(basePath + MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_FILE, PPS.O_RDONLY);
		}
		
		private function controllerConnect():void
		{
			if(this._ppsController)
			{
				this._ppsController.connect(this.basePath + MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_CONTROLLER, false);
			}
		}

		private function controllerEvent(event:PPSChannelEvent):void
		{
			switch(event.type)
			{
				case PPSChannelEvent.OPEN:
					this.isMediaPlayerRunning = true;
					break;
				case PPSChannelEvent.OPEN_FAIL:
					// This could either mean that the server isn't up, or the control object
					// simply doesn't exist. Either way, we need to try this again in an
					// arbitrarily small amount of time so that we can be aware of when this
					// device becomes available.
					this.startControllerReconnectTimer();
					break;
				case PPSChannelEvent.CONNECT:
					break;
				case PPSChannelEvent.DISCONNECT:
					this.isMediaPlayerRunning = false;
					// Well, the device has gone down, but it doesn't mean it won't be coming
					// back up, so we need to start polling for the control object again
					this.startControllerReconnectTimer();
					break;
				case PPSChannelEvent.CLOSE:
					break;
			}
		}
		
		private function startControllerReconnectTimer():void
		{
			// Of course we only want to do this if the timer isn't already running
			if(this._ppsControllerReconnectTimer == null)
			{
				this._ppsControllerReconnectTimer = new Timer(PPS_CONTROLLER_RECONNECT_DELAY,1);
				this._ppsControllerReconnectTimer.addEventListener(TimerEvent.TIMER, this.handleControllerReconnectTimer);
				this._ppsControllerReconnectTimer.start();
			}
		}
		
		private function disposeControllerReconnectTimer():void
		{
			if(this._ppsControllerReconnectTimer != null)
			{
				this._ppsControllerReconnectTimer.stop();
				this._ppsControllerReconnectTimer.removeEventListener(TimerEvent.TIMER, this.handleControllerReconnectTimer);
				this._ppsControllerReconnectTimer = null;
			}
		}
		
		private function handleControllerReconnectTimer(event:TimerEvent):void
		{
			// Initiate the connect
			this.controllerConnect();
			
			// Stop the timer and clean up
			this.disposeControllerReconnectTimer();
		}
		
		private function setMediaPlayerContext(event:PPSEvent):void
		{
			// Get the player context from the PPS data
			this._mediaPlayerContextName = (event.currentTarget as PPS).data.context != "" ? (event.currentTarget as PPS).data.context : null;
			
			// Now that we have the player context, we can start listening to the PPS that contains the 'now playing' information
			if(this._mediaPlayerContextName)
			{
				// Also initialize a PPS Listener to get the status of the media player (status and state PPS files)
				this.initializePlayerStatusListener();
				
				this.dispatchEvent(new DeviceEvent(DeviceEvent.CONTEXT_NAME_UPDATED, this));
			}
		}
				
		private function initializePlayerStatusListener():void
		{
			this.playerStatus = new MediaPlayerStatus();
			
			// Close existing handler if it exists
			if(this.statePps != null)
			{
				this.statePps.close();
				this.statePps = null;
			}
			this.statePps = new PPS();
			
			// State PPS (Lets us know if the player is playing or paused)
			// Create the handlers
			var stateChangedHandler:Function = function(event:PPSEvent):void
			{
				var speed:uint = uint((event.currentTarget as PPS).data.speed);
				var state:String = (event.currentTarget as PPS).data.state;
				
				if((state != null && (state == "idle" || state == "stopped"))
					|| speed == 0)
				{
					playerStatus.isPlaying = false;
					
					// Also clear the now playing information if the playback has stopped
					/*if(state == "stopped")
					{
						nowPlaying = null;
					}*/
				}
				else
				{
					playerStatus.isPlaying = true;
				}
			};
			var statePpsRemoved:Function = function(event:PPSEvent):void
			{
				statePps.close();
				statePps.removeEventListener(PPSEvent.PPS_OPENED, stateChangedHandler);
				statePps.removeEventListener(PPSEvent.PPS_CHANGED, stateChangedHandler);
				statePps.removeEventListener(PPSEvent.PPS_REMOVED, statePpsRemoved);
				statePps = null;
			};
			
			this.statePps.addEventListener(PPSEvent.PPS_OPENED, stateChangedHandler);
			this.statePps.addEventListener(PPSEvent.PPS_CHANGED, stateChangedHandler);
			// FIXME: This PPS listener should be cleaned up in the cleanUp method
			//this.statePps.addEventListener(PPSEvent.PPS_REMOVED, statePpsRemoved);
			
			if(new File(basePath + MediaConstants.PLAYER_STATE_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName)).exists)
			{
				this.openStatePps();
			}
			else
			{
				this.startStateMonitor();
			}

			if(this.statusPps != null)
			{
				this.statusPps.close();
				this.statusPps = null;
			}
			this.statusPps = new PPS();

			// Status PPS (Gives us the current position in the track)
			var statusChangedHandler:Function = function(event:PPSEvent):void
			{
				if((event.currentTarget as PPS).data.position != null)
				{
					// The position has a format of n:pos where n is 1 (possibly track in playlist?) and
					// pos is the position in the track in milliseconds
					var aryPosition:Array = String((event.currentTarget as PPS).data.position).split(":");
					if(aryPosition.length == 2)
					{
						playerStatus.position = uint(aryPosition[1]);
					}
				}
				else
				{
					playerStatus.position = 0;
				}
			};
			var statusPpsRemoved:Function = function(event:PPSEvent):void
			{
				statusPps.close();
				statusPps.removeEventListener(PPSEvent.PPS_OPENED, statusChangedHandler);
				statusPps.removeEventListener(PPSEvent.PPS_CHANGED, statusChangedHandler);
				statusPps.removeEventListener(PPSEvent.PPS_REMOVED, statusPpsRemoved);
				statusPps = null;
			};
	
			this.statusPps.addEventListener(PPSEvent.PPS_OPENED, statusChangedHandler);
			this.statusPps.addEventListener(PPSEvent.PPS_CHANGED, statusChangedHandler);
			// FIXME: This PPS listener should be cleaned up in the cleanUp method
			//this.statusPps.addEventListener(PPSEvent.PPS_REMOVED, statusPpsRemoved);
			
			
			if(new File(basePath + MediaConstants.PLAYER_STATUS_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName)).exists)
			{
				this.openStatusPps();
			}
			else
			{
				this.startStatusMonitor();
			}
		}
		
		private function startStateMonitor():void
		{
			stateMonitor = new FileMonitor();
			stateMonitor.addEventListener(FileMonitorEvent.CREATE, openStatePps);
			stateMonitor.addEventListener(FileMonitorEvent.CHANGE, openStatePps);
			stateMonitor.file = new File(basePath + MediaConstants.PLAYER_STATE_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName));
			stateMonitor.watch();
		}
		
		private function stopStateMonitor():void
		{
			stateMonitor.removeEventListener(FileMonitorEvent.CREATE, openStatePps);
			stateMonitor.removeEventListener(FileMonitorEvent.CHANGE, openStatePps);
			stateMonitor.unwatch();
			stateMonitor = null;
		}		
		
		private function openStatePps(event:Event=null):void
		{
			statePps.openAsync(basePath + MediaConstants.PLAYER_STATE_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName), PPS.O_RDONLY);

			if(this.stateMonitor != null)
			{
				this.stopStateMonitor();
			}
		}

		private function startStatusMonitor():void
		{
			statusMonitor = new FileMonitor();
			statusMonitor.addEventListener(FileMonitorEvent.CREATE, openStatusPps);
			statusMonitor.addEventListener(FileMonitorEvent.CHANGE, openStatusPps);
			statusMonitor.file = new File(basePath + MediaConstants.PLAYER_STATUS_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName));
			statusMonitor.watch();
		}
		
		private function stopStatusMonitor():void
		{
			statusMonitor.removeEventListener(FileMonitorEvent.CREATE, openStatusPps);
			statusMonitor.removeEventListener(FileMonitorEvent.CHANGE, openStatusPps);
			statusMonitor.unwatch();
			statusMonitor = null;
		}		

		private function openStatusPps(event:Event=null):void
		{
			statusPps.openAsync(basePath + MediaConstants.PLAYER_STATUS_PPS_PATH.replace("[INSTANCEID]", _mediaPlayerContextName), PPS.O_RDONLY);

			if(this.statusMonitor != null)
			{
				this.stopStatusMonitor();
			}
		}
		
		private function initializeNowPlayingListener():void
		{
			infoPPS = new PPS();
			
			var thisDevice:Device = this;	// Needed for the anon function since 'this' is not available
			var nowPlayingChangeHandler:Function = function(event:PPSEvent):void
			{
				var mediaType:String = "";
				if((event.currentTarget as PPS).data.nowPlaying)
				{
					nowPlaying = new NowPlaying
					(
						(event.currentTarget as PPS).data.nowPlaying.title,
						(event.currentTarget as PPS).data.nowPlaying.artist,
						(event.currentTarget as PPS).data.nowPlaying.album,
						(event.currentTarget as PPS).data.nowPlaying.duration,
						(event.currentTarget as PPS).data.nowPlaying.art,
						(event.currentTarget as PPS).data.nowPlaying.mediaType
					);
				}
				dispatchEvent(new DeviceEvent(DeviceEvent.EVENT_DEVICE_NOWPLAYING_UPDATED, thisDevice));
			};
			var nowPlayingPpsRemoved:Function = function(event:Event = null):void
			{
				infoPPS.close();
				nowPlaying = null;
				
				// Since the NowPlaying PPS file has been removed, we can assume that playback
				// of the device's playlist has completed
				dispatchEvent(new DeviceEvent(DeviceEvent.EVENT_DEVICE_NOWPLAYING_REMOVED, thisDevice));

				// Start the monitor so we can update the next time the file is created
				startNowPlayingPpsMonitor();
			}
			
			infoPPS.addEventListener(PPSEvent.PPS_OPENED, nowPlayingChangeHandler);
			infoPPS.addEventListener(PPSEvent.PPS_CHANGED, nowPlayingChangeHandler);
			infoPPS.addEventListener(PPSEvent.PPS_REMOVED, nowPlayingPpsRemoved);
			
			if(new File(basePath + MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE).exists)
			{
				// We can open the PPS file right away
				this.openNowPlayingPps();
			}
			else
			{
				// Need to create a file monitor and wait for it to appear
				this.startNowPlayingPpsMonitor();
			}
		}
		
		private function openNowPlayingPps(event:Event=null):void
		{
			infoPPS.openAsync(basePath + MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE, PPS.O_RDONLY);
			
			// Stop the monitor, if it's running, since it's no longer needed
			if(this.nowPlayingMonitor != null)
			{
				this.stopNowPlayingPpsMonitor();
			}
		}
		
		private function startNowPlayingPpsMonitor():void
		{
			nowPlayingMonitor = new FileMonitor();
			nowPlayingMonitor.addEventListener(FileMonitorEvent.CREATE, openNowPlayingPps);
			nowPlayingMonitor.addEventListener(FileMonitorEvent.CHANGE, openNowPlayingPps);
			nowPlayingMonitor.file = new File(basePath + MediaConstants.OCTANE_MEDIA_PPS_PATH + MediaConstants.OCTANE_MEDIA_PPS_NOWPLAYING_FILE);
			nowPlayingMonitor.watch();
		}
		
		private function stopNowPlayingPpsMonitor():void
		{
			nowPlayingMonitor.removeEventListener(FileMonitorEvent.CREATE, openNowPlayingPps);
			nowPlayingMonitor.removeEventListener(FileMonitorEvent.CHANGE, openNowPlayingPps);
			nowPlayingMonitor.unwatch();
			nowPlayingMonitor = null;
		}
		
		public function dispose():void
		{
			// Close the PPS controller channel and halt the timer if it's running
			if(this._ppsController != null)
			{
				this._ppsController.disconnect();
				this._ppsController = null;
			}
			if(this._ppsControllerReconnectTimer != null)
			{
				this.disposeControllerReconnectTimer();
			}
			
			if(this.devicePPS)
			{
				this.devicePPS.close();
			}
			if(this.infoPPS)
			{
				this.infoPPS.close();
			}
			if(this.statusPps)
			{
				this.statusPps.close();
			}
			if(this.statePps)
			{
				this.statePps.close();
			}
			
			if(this.nowPlayingMonitor)
			{
				this.stopNowPlayingPpsMonitor();	
			}
			if(this.statusMonitor)
			{
				this.stopStatusMonitor();
			}
			if(this.stateMonitor)
			{
				this.stopStateMonitor();
			}
			
			if(this.volumeInfo)
			{
				this.volumeInfo.close();
			}
		}
	}
}