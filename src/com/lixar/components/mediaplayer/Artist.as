package com.lixar.components.mediaplayer
{
	import spark.components.Image;

	[Bindable]
	public class Artist
	{
		public var id:uint;
		public var name:String;
		public var thumbnail:Image;
		public var thumbId:uint;
		public var totalAlbums:uint = 0;
		public var totalTracks:uint = 0;
		public var totalTrackDuration:uint = 0;
		
		public function Artist()
		{
		}
	}
}