package com.lixar.components
{
	import qnx.events.PPSEvent;
	import qnx.pps.PPS;

	public class PlaybookDisplay
	{
		private static const displayPPS:String = "/pps/services/power/display";
		
		public function PlaybookDisplay()
		{
		}
		
		// set screen brightness, values from 0 to 255
		public static function setBrightness(brightness:uint):void
		{
			brightness = Math.min(brightness, 255); // make sure we're not > 255
			var pps:PPS = new PPS();
			pps.addEventListener
			(
				PPSEvent.PPS_OPENED,
				function(event:PPSEvent):void
				{
					(event.currentTarget as PPS).data.backlightLevel = brightness;
					(event.currentTarget as PPS).data.backlightLevelCharging = brightness;
					(event.currentTarget as PPS).write((event.currentTarget as PPS).data);
					(event.currentTarget as PPS).close();
				}
			);
			pps.openAsync(displayPPS, PPS.O_WRONLY);
		}
		
		public static function getBrightness():uint
		{
			var pps:PPS = new PPS();
			pps.open(displayPPS, PPS.O_RDONLY);
			var brightness:uint = pps.data.backlightLevelCharging;
			pps.close();
			return brightness;
		}
			
	}
}