package com.lixar.components
{
	
	import flash.events.Event;
	
	import mx.core.UIComponent;
	import mx.events.MoveEvent;
	
	import qnx.ui.events.SliderEvent;
	import qnx.ui.slider.VolumeSlider;

	[Event(name="move", type="mx.events.MoveEvent")]
	
	public class VolumeSlider extends UIComponent
	{	
		private var slider:qnx.ui.slider.VolumeSlider = new qnx.ui.slider.VolumeSlider;
		
		public function VolumeSlider()
		{
			super();
			addChild(slider);
			height = slider.height;
			slider.addEventListener(SliderEvent.MOVE, sliderMove);
		}
		
		override public function set width(value:Number):void
		{
			super.width = value;
			slider.width = value;
		}
		
		override public function set height(value:Number):void
		{
			super.height = value;
			slider.height = value;
		}
		
		public function set value(value:Number):void
		{
			slider.value = value;
		}
		
		public function get value():Number
		{
			return slider.value;
		}
		
		private function sliderMove(event:Event):void
		{
			dispatchEvent(new MoveEvent("move"));
		}
	}
}