/*
	Singleton base class
	kmccann@lixar.com
	extend to enforce singleton style behaviour in a class
	extend Singleton in your class, then instantiate with:
	yourClass:YourClass = Singleton.getInstance(YourClass) as YourClass;
*/
package com.lixar.util
{
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;

	public class Singleton
	{
		private static var instances:Dictionary = new Dictionary();
		private static var enforcer:SingletonEnforcer;
		
		public function Singleton()
		{
			if(enforcer == null)
			{				
				throw(new Error("Classes extending Singleton can only be instantiated by the getInstance method"));				
			}
			var className:String = getQualifiedClassName(this);
			var classObj:Class = getDefinitionByName(className) as Class;
			if(classObj == Singleton)
			{
				throw(new Error("Singleton is a base class that cannot be instantiated"));
			}
			var instance:Object = instances[classObj];
			if(instance != null)
			{				
				throw(new Error("Classes extending Singleton can only be instantiated once by the getInstance method"));				
			}
			else
			{				
				instances[classObj] = this;				
			}
		}
		
		public static function getInstance(classObj:Class):Object			
		{			
			var instance:Object = instances[classObj];			
			if(instance == null)
			{
				enforcer = new SingletonEnforcer();
				instance = Object(new classObj());
				enforcer = null;
				var singleton:Singleton = instance as Singleton;
				instance = singleton;
				if(singleton == null)
				{					
					throw(new Error("getInstance can only be called for Classes extending Singleton"));					
				}				
			}			
			return instance;			
		}
	}
}

class SingletonEnforcer {}