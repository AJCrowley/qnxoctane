package com.lixar.util
{
	import com.lixar.components.mediaplayer.AudioFile;

	public class MediaUtils
	{
		public function MediaUtils()
		{
		}

		public static function getTotalDuration(tracks:Vector.<AudioFile>):String
		{
			var totalMSec:uint = 0;
			for each (var track:AudioFile in tracks)
			{
				totalMSec += track.duration;
			}
			return MediaUtils.getFormattedTime(totalMSec);
		}
		
		public static function getFormattedTime(milliseconds:uint):String
		{
			var seconds:uint = milliseconds / 1000;
			var strFormattedTime:String = "0:00";
			
			if (seconds > 0)
			{
				// get the minutes and hours as whole numbers
				var minutes:Number = Math.floor(seconds / 60);
				var hours:Number = Math.floor(seconds / 60 / 60);
				
				if (hours)
				{
					// remove the extra minutes from the hours
					minutes = minutes % 60;
					strFormattedTime = hours.toString() + ":" + (minutes < 10 ? "0" : "") + minutes.toString();
				}
				else
				{
					strFormattedTime = minutes.toString();
				}
				
				// remove any whole minutes to get our seconds count
				var formattedSecs:String = String(seconds % 60); 
				// zero-pad seconds if they're single digit
				strFormattedTime += ":" + (formattedSecs.length == 1 ? "0" : "") + formattedSecs;
			}
			
			return strFormattedTime;
		}
		
		
	}
}