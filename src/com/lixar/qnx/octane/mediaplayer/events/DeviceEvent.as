package com.lixar.qnx.octane.mediaplayer.events
{
	import com.lixar.components.mediaplayer.Device;
	
	import flash.events.Event;
	
	public class DeviceEvent extends Event
	{
		public static const ADDED:String = "deviceAdded";
		public static const REMOVED:String = "deviceRemoved";
		public static const CONTEXT_NAME_UPDATED:String = "contextNameUpdated";
		public static const EVENT_DEVICE_NOWPLAYING_UPDATED:String = "nowPlayingUpdated";
		public static const EVENT_DEVICE_NOWPLAYING_REMOVED:String = "nowPlayingRemoved";
		
		private var _device:Device;
		public function get device():Device { return this._device; }
		
		public function DeviceEvent(type:String, device:Device, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this._device = device;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new DeviceEvent(this.type, this.device, this.bubbles, this.cancelable);
		}
		
		override public function toString():String
		{
			return formatToString("DeviceEvent", "type", "bubbles", "cancelable", "device");
		}
	}
}