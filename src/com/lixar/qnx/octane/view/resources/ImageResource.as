package com.lixar.qnx.octane.view.resources
{
	public class ImageResource
	{
		[Embed(source="resources/topbar_ico_back.png")]
		public static const TOPBAR_ICON_BACK:Class;
		
		[Embed(source="resources/topbar_ico_dim.png")]
		public static const TOPBAR_ICON_DIM:Class;
		
		[Embed(source="resources/album_cover_blank_small.png")]
		public static const MEDIAPLAYER_DEFAULT_AUDIO_THUMBNAIL:Class;

		[Embed(source="resources/default_video_thumbnail.png")]
		public static const MEDIAPLAYER_DEFAULT_VIDEO_THUMBNAIL:Class;

		[Embed(source="resources/skin/GlyphFullscreen.png")]
		public static const MEDIAPLAYER_BUTTON_FULLSCREEN:Class;

		[Embed(source="resources/skin/GlyphUnFullscreen.png")]
		public static const MEDIAPLAYER_BUTTON_UNFULLSCREEN:Class;

		[Embed(source="resources/skin/GlyphVolume.png")]
		public static const MEDIAPLAYER_BUTTON_VOLUME:Class;

		[Embed(source="resources/skin/GlyphBrowse.png")]
		public static const MEDIAPLAYER_BUTTON_BROWSE:Class;

		[Embed(source="resources/skin/GlyphNet.png")]
		public static const MEDIAPLAYER_BUTTON_NETWORK:Class;

		[Embed(source="resources/skin/GlyphPlay.png")]
		public static const MEDIAPLAYER_BUTTON_PLAY:Class;

		[Embed(source="resources/skin/GlyphPause.png")]
		public static const MEDIAPLAYER_BUTTON_PAUSE:Class;
		
		[Embed(source="resources/node_nowplaying_center.png")]
		public static const MULTI_NODE_NOWPLAYING_CENTER:Class;
		
		[Embed(source="resources/node_nowplaying_rearleft.png")]
		public static const MULTI_NODE_NOWPLAYING_REAR_LEFT:Class;

		[Embed(source="resources/node_nowplaying_rearright.png")]
		public static const MULTI_NODE_NOWPLAYING_REAR_RIGHT:Class;
		
		[Embed(source="resources/node_nowplaying_selected.png")]
		public static const MULTI_NODE_NOWPLAYING_SELECTED_BG:Class;
		
		[Embed(source="resources/node_nowplaying_currentdevice.png")]
		public static const MULTI_NODE_NOWPLAYING_CURRENT_DEVICE:Class;
		
		[Embed(source="resources/node_nowplaying_ico_music.png")]
		public static const MULTI_NODE_NOWPLAYING_ICON_MUSIC:Class;
		
		[Embed(source="resources/node_nowplaying_ico_video.png")]
		public static const MULTI_NODE_NOWPLAYING_ICON_VIDEO:Class;
		
		[Embed(source="resources/node_nowplaying_ico_pandora.png")]
		public static const MULTI_NODE_NOWPLAYING_ICON_PANDORA:Class;

		[Embed(source="resources/vehicle.png")]
		public static const MULTI_NODE_VEHICLE:Class;
		
		[Embed(source="resources/skin/OctaneSliderThumb.png")]
		public static const TRACKING_SLIDER_THUMB:Class;
		
		[Embed(source="resources/skin/OctaneSliderTrackOff.png", scaleGridLeft="48", scaleGridRight="236", scaleGridTop="27", scaleGridBottom="45")]
		public static const TRACKING_SLIDER_TRACK:Class;
		
		[Embed(source="resources/skin/OctaneSliderTrackOn.png", scaleGridLeft="48", scaleGridRight="236", scaleGridTop="27", scaleGridBottom="45")]
		public static const TRACKING_SLIDER_TRACK_HIGHLIGHT:Class;
		
		public function ImageResource()
		{
		}
	}
}