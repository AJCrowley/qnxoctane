package com.lixar.skins
{
	import com.lixar.qnx.octane.view.resources.ImageResource;
	
	import mx.core.BitmapAsset;
	
	import spark.skins.mobile.HSliderThumbSkin;
	
	public class HSliderThumbSkin extends spark.skins.mobile.HSliderThumbSkin
	{
		private var sliderNormal:BitmapAsset = new ImageResource.TRACKING_SLIDER_THUMB();
		
		private var displayedState:String;
		
		public function HSliderThumbSkin()
		{
			super();
			thumbImageWidth = 72;
			thumbImageHeight = 72;
		}
		
		override protected function commitCurrentState():void
		{
			if(!currentThumbSkin)
			{
				currentThumbSkin = sliderNormal;
				addChild(currentThumbSkin);
			}
			displayedState = currentState;
			invalidateDisplayList();
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
		{
			// put in a larger hit zone than the thumb
			graphics.beginFill(0xffffff, 0);
			graphics.drawRect(-hitZoneOffset, -hitZoneOffset, hitZoneSideLength, hitZoneSideLength);
			graphics.endFill();
		}
	}
}