package com.lixar.skins
{
	import spark.skins.mobile.HSliderSkin;
	import spark.skins.mobile.HSliderTrackSkin;
	
	public class HSliderGraphicSkin extends spark.skins.mobile.HSliderSkin
	{
		public function HSliderGraphicSkin()
		{
			super();
			thumbSkinClass = com.lixar.skins.HSliderThumbSkin;
			trackSkinClass = com.lixar.skins.HSliderTrackSkin;
		}
	}
}