package com.lixar.skins
{
	import com.lixar.qnx.octane.view.resources.ImageResource;
	import com.lixar.skins.porsche.resources.sliderTrackOff;
	
	import flash.display.GradientType;
	import flash.display.InterpolationMethod;
	import flash.display.SpreadMethod;
	import flash.geom.Matrix;
	
	import mx.core.BitmapAsset;
	
	import spark.skins.mobile.HSliderTrackSkin;
	
	public class HSliderTrackSkin extends spark.skins.mobile.HSliderTrackSkin
	{
		public function HSliderTrackSkin()
		{
			super();
			trackHeight = 72;
			trackWidth = 284;
		}
		
		override protected function createChildren():void
		{
			trackSkin = new ImageResource.TRACKING_SLIDER_TRACK;
			addChild(trackSkin);
		}
		
		override protected function drawBackground(unscaledWidth:Number, unscaledHeight:Number):void
		{        
			var unscaledTrackWidth:int = unscaledWidth - (2 * visibleTrackOffset);
			
			// draw the round rect
			graphics.beginFill(getStyle("chromeColor"), 0);
			graphics.drawRoundRect(visibleTrackOffset, 0,
				unscaledTrackWidth, trackHeight,
				trackHeight, trackHeight);
			graphics.endFill();
		}
	}
}