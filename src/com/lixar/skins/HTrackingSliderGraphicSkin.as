package com.lixar.skins
{
	/*import spark.skins.mobile.HSliderSkin;
	import spark.skins.mobile.HSliderTrackSkin;*/
	
	public class HTrackingSliderGraphicSkin extends HSliderSkin
	{
		public function HTrackingSliderGraphicSkin()
		{
			super();
			thumb.setStyle("skinClass", com.lixar.skins.HSliderThumbSkin);
			track.setStyle("skinClass", com.lixar.skins.HSliderTrackSkin);
			trackHighLight.setStyle("skinClass", com.lixar.skins.HSliderTrackHighlightGraphicSkin);
			/*thumbSkinClass = com.lixar.skins.HSliderThumbSkin;
			trackSkinClass = com.lixar.skins.HSliderTrackSkin;*/
		}
	}
}